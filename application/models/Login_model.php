<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Login_model extends CI_Model {
	protected $table='login';
    public function __construct() {

        parent::__construct();
        $this->load->database();
		$this->load->library('session');
    }
    
    public function resolve_user_login($username, $password) {

        $this->db->select('password');
        $this->db->from('login');
        $this->db->where('userName', $username);
        $hash = $this->db->get()->row('password');

        return $this->verify_password_hash($password, $hash);

    }

    /**
     * get_user_id_from_username function.
     *
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_user_id_from_username($username) {

        $this->db->select('id');
        $this->db->from('login');
        $this->db->where('username', $username);

        return $this->db->get()->row('id');

    }

    /**
     * get_user function.
     *
     * @access public
     * @param mixed $user_id
     * @return object the user object
     */
    public function get_user($user_id) {

        $this->db->from('login');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();

    }

    public function updatePassword ($user_id, $password) {
        $data = ["password" => $this->hash_password($password)];
        $this->db->set($data);
        $this->db->where('id', $user_id);
        if ($this->db->update('login')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Public method to check password match using the private method verify_password_hash
     *
     * @access public
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    public function checkPasswordMatch ($password, $hash) {
        return $this->verify_password_hash($password, $hash);
    }


    /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);

    }

    /**
     * verify_password_hash function.
     *
     * @access private
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    private function verify_password_hash($password, $hash) {

        return password_verify($password, $hash);

    }
    //branch credential
    public function insertBranchData($params2)
   	{
   		$params2['password']= $this->hash_password($params2['password']);
		$ins	=	$this->db->insert('login',$params2);
		return $ins;
		
	}
	public function deleteBranchData($id) { 
         if ($this->db->delete($this->table, "branchId = ".$id)) { 
            return true; 
         } 
      } 
      
      //validation for checking the username is existing or not
      function user_exists($userName)
		{
		    $this->db->where('userName',$userName);
		    $query = $this->db->get('login'); //echo  $query;die;
		    $num = $query->num_rows(); //echo $num;
		    return $num;
		}
      
      //getting all branch data for password reset
      public function getAllData()
      {
	  	$this->db->select('branch.*');
	  	$this->db->from('branch');
	  	$query  = $this->db->get();
	  	return $query->result();
	  }
	  
	  //resetting old branch password to new
	  public function updateBranchPass($branchId, $password) {
        $data = ["password" => $this->hash_password($password)];
        $this->db->set($data);
        $this->db->where('branchId', $branchId);
        if ($this->db->update('login')) {
            return true;
        } else {
            return false;
        }
    }
	  
}