<?php 
   class Point_setting_model extends CI_Model {
	protected $table='point_setting';
      function __construct() { 
         parent::__construct(); 
      } 
   	     
     public function getUpdateData($params)
     { 
     	$this->db->select($params['fields']);
		$query=$this->db->get_where($this->table,$params['condition']);
		//echo var_dump($query);
		return $query->result_array();		
	 }
	 public function updateAction($params,$editId)
	 {
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up	=	$this->db->update($this->table,$params);	
		return $up;
	 }
	  public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
   } 
