<?php 
   class Customer_model extends CI_Model {
	protected $table='customer';
      function __construct() { 
         parent::__construct(); 
      } 
   	
   	public function insertData($params)
   	{
		$ins	=	$this->db->insert($this->table,$params);
		return $ins;
	}
	
	public function getData($params)
	{
		$this->db->select($params['fields']);
		$this->db->order_by($params['order']);
		$query=$this->db->get_where($this->table);//, $params['condition']
		return $query->result_array();
	}
     
     public function getUpdateData($params)
     { 
     	$this->db->select($params['fields']);
		$query=$this->db->get_where($this->table,$params['condition']);
		//echo var_dump($query);
		return $query->result_array();		
	 }
	 public function updateAction($params,$editId)
	 {
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up	=	$this->db->update($this->table,$params);	
		return $up;
	 }
	  public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
      
      ///for checking mobile already is there or not 
      public function mobile_exists($no)
      {
	  	$this->db->where('mobile',$no);
	  	$query = $this->db->get('customer'); //echo  $query;die;
	    $num = $query->num_rows(); //echo $num;die;
	    return $num;
	  }
   } 
