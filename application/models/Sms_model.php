<?php 
   class Sms_model extends CI_Model {
	protected $table='customer';
	protected $table1='sms_send_details';
      function __construct() { 
         parent::__construct(); 
      } 
   	function getSearchData($data){
            $loginType = $_SESSION['user_type'];
   		    $loginId = $_SESSION['user_id'];
	   		$cardId			=	$data['cardId'];
	        $customerName	=	$data['customerName'];
	        $phone			=	$data['phone'];
                $where = '1';
	        
		    $this->db->select('card.*,customer.*,customer.ID as customerId,card.ID as card_id ');
			$this->db->from('card');
			$this->db->join('customer','card.customerId = customer.ID');
			
			if($customerName)
			{
				$this->db->where('customer.customerName', $customerName);
			}

            if($customerName && $phone)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone) OR (customer.customerName='$customerName' AND customer.mobile=$phone))";
				
			}

			if($phone && !$customerName)
			{
				$where = "(customer.phone='$phone' OR customer.mobile='$phone')";
			}
			if($cardId)
			{
				$this->db->where('card.cardId', $cardId);
			}
			if($cardId && $phone)
			{
				$where = "((card.cardId='$cardId' AND customer.phone=$phone) OR (card.cardId='$cardId' AND customer.mobile=$phone))";
			}
			
			if($loginType!="admin") {
				$where = "$where and customer.loginId='$loginId'";
			    $this->db->where($where);
			    $this->db->order_by('customer.ID');
			}
			else 
			{
            if($where!=1)
			{
				$this->db->where($where);
			}
				$this->db->order_by('customer.ID');
			}
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }
	function getSms($id){
	   			        
		    $this->db->select('sms_setting.sms,sms_setting.type');
			$this->db->from('sms_setting');
			$this->db->where('sms_setting.type', $id);	
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }  	
	function getSmsBroadcast($id)
	{
		$this->db->select('sms_setting.sms,sms_setting.type');
			$this->db->from('sms_setting');
			$this->db->where('sms_setting.type', $id);	
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	}
	
	 public function saveSmsData($params) { 
         $ins	=	$this->db->insert($this->table1,$params);
		 return $ins;
      } 
     public function saveBroadData($params) { 
         $ins	=	$this->db->insert($this->table1,$params);
		 return $ins;
      }

     //sms sent details  deletion
	  public function delete_sms_sent_details($id){
	  	if ($this->db->delete($this->table1, "ID = ".$id)) { 
            return true; 
         }
	  }   
	
   } 
