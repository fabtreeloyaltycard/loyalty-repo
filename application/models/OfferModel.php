<?php
 class OfferModel extends CI_Model {
	protected $table='offer_details';
	function __construct() { 
         parent::__construct(); 
      } 
      
	public function insertData($params) { 
         $ins	=	$this->db->insert($this->table,$params);
		 return $ins;
      } 
      
    
    public function getData($params)
	{
		$this->db->select($params['fields']);
		$this->db->order_by($params['order']);
		$query=$this->db->get_where($this->table);//, $params['condition']
		return $query->result_array();
	}
    
    public function getUpdateData($params)
     { 
     	$this->db->select($params['fields']);
		$query=$this->db->get_where($this->table,$params['condition']);
		//echo var_dump($query);
		return $query->result_array();		
	 }
    
	public function updateAction($params,$editId)
	 {
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up	=	$this->db->update($this->table,$params);
		return $up;
	 }
	 
	 
	  public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
      
      //function if offer is already exists
      public function offer_exists($offer){
	  	$this->db->where('offer',$offer);
	  	$query= $this->db->get('offer_details');
	  	$num = $query->num_rows();
	  	return $num;
	  }

} 
?>