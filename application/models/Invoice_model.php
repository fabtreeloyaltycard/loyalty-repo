<?php 
   class Invoice_model extends CI_Model {
	protected $table1			=	'point_details';
	protected $table2			=	'customer';
	protected $table3			=	'card';
        protected $table4			=	'sms_send_details';

      function __construct() { 
         parent::__construct(); 
      }
      
	   	function getSearchData($data){
	   		$cardId			=	$data['cardId'];
	        $customerName	=	$data['customerName'];
	        $phone			=	$data['phone'];
	        
		    $this->db->select('card.*,customer.*,customer.ID as customerId,card.ID as card_id ');
			$this->db->from('card');
			$this->db->join('customer','card.customerId = customer.ID');
			
		
			if($customerName && $phone)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone) OR (customer.customerName='$customerName' AND customer.mobile=$phone))";
				
			}
			
            if($phone && !$customerName)
			{
				$where = "(customer.phone='$phone' OR customer.mobile='$phone')";
			}
                  
			if($cardId)
			{
				
				$where = "(card.cardId=$cardId)";
			}
               
            if($customerName && $phone && $cardId)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone AND card.cardId=$cardId) OR (customer.customerName='$customerName' AND customer.mobile=$phone AND card.cardId=$cardId))";
			}
			if($cardId && $phone)
			{
				$where = "((card.cardId='$cardId' AND customer.phone=$phone) OR (card.cardId='$cardId' AND customer.mobile=$phone))";
			}
			
			$this->db->where($where);
			$this->db->order_by('card.ID');
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }
	
		function pointSum($cardId){
	   		
		    $this->db->select('sum(point)as totalpoint');
			$this->db->from('point_details');
			
			$this->db->where('point_details.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();
			$data = $query->row();
			if($data)
			{
				$result = $data->totalpoint;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
	    function redeemSum($cardId){
	   		
		    $this->db->select('sum(redeemPoint)as totalredeem');
			$this->db->from('redeem');
			
			$this->db->where('redeem.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			$data = $query->row();
			if($data)
			{
				$result = $data->totalredeem;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
             function purchaseReturnSum($cardId){
	   		
		    $this->db->select('sum(point)as totalPurReturn');
			$this->db->from('purchase_return');
			
			$this->db->where('purchase_return.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			$data = $query->row();
			if($data)
			{
				$result = $data->totalPurReturn;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
            
	 public function insertData($params)
   	{
		$ins	=	$this->db->insert($this->table1,$params);
		$lastInsertId = $this->db->insert_id();
		return $lastInsertId;
	}
	public function getAllData()
	{
        $loginType = $_SESSION['user_type'];
		$loginId = $_SESSION['user_id'];

        $customerName = $this->input->post('customerName');
    	$phone = $this->input->post('phone');
    	$cardNo  = $this->input->post('cardNo');
    	$addedDate = $this->input->post('addedDate');
    	$where = '1';
    	if($customerName)
    	{
       	$this->db->where($this->table2.'.customerName', $customerName);
    	}
    	if($phone)
    	{
       	$where = "(customer.phone like'%$phone%' OR customer.mobile like '%$phone%')"; 
    	}
		if($cardNo)
    	{
       	$this->db->where($this->table3.'.cardId', $cardNo);
    	} 
    	
    	if($addedDate)
        	{
           	$addedDate = date('Y-m-d', strtotime($addedDate));
            $this->db->where('point_details.addedDate', $addedDate);
        	}
                    
		$this->db->select('card.*,customer.*,point_details.*,customer.ID as customerId,card.ID as card_id,card.cardId as cardNo,point_details.ID as point_details_id ');
		$this->db->from('card');
		$this->db->join('customer','card.customerId = customer.ID');
		$this->db->join('point_details','point_details.cardId = card.ID');
		
		if($loginType!="admin") {
			$where = "$where and card.loginId='$loginId'";
			$this->db->where($where);
			$this->db->order_by('point_details.ID');
		}
		else {
				
			  if($where!=1)
				{
					$this->db->where($where);
				}
					
			$this->db->order_by('point_details.ID');
		}

		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	 public function deleteData($id) { 
         if ($this->db->delete($this->table1, "ID = ".$id)) { 
            return true; 
         } 
      }
      
      //for deleting the card details in point details table
      /*public function deleteCardData($id) { 
         if ($this->db->delete($this->table1, "cardId = ".$id)) { 
            return true; 
         } 
      }*/
      
     //for deleting the customer details in point details table
     public function deleteCusData($id) { 
         if ($this->db->delete($this->table1, "customerId = ".$id)) { 
            return true; 
         } 
      } 	

function getSmsSearchData($lastInsertCardId){
	   			        
		    $this->db->select('card.*,customer.*,point_details.*,card.cardId as cardNumber');
			$this->db->from('card','customer','point_details');
			$this->db->join('customer','card.customerId = customer.ID');
			$this->db->join('point_details','point_details.cardId = card.ID');
						
			$this->db->where('point_details.ID', $lastInsertCardId);
						
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    }
	    function getSms($id){
	   			        
		    $this->db->select('sms_setting.sms,sms_setting.type');
			$this->db->from('sms_setting');
			$this->db->where('sms_setting.type', $id);	
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }  
	 
           public function saveData($params) { 
         $ins	=	$this->db->insert($this->table4,$params);
		 return $ins;
      }  		
   } 
 