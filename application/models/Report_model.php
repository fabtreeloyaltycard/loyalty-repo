<?php 
   class Report_model extends CI_Model {
	protected $table1			=	'redeem';
	protected $table2			=	'customer';
	protected $table3			=	'card';
	protected $table4			=	'branch';
      function __construct() { 
         parent::__construct(); 
      }
      
	   	function getredeemData(){
	   		$loginId = $_SESSION['user_id']; //print_r($loginId);die;
	   		$loginType = $_SESSION['user_type']; //print_r($loginId);die;
	   		//$branchId = $data['branchId'];
	   		$phoneNo = $this->input->post('phoneNo');
	   		$customerName = $this->input->post('customerName');
        	$cardNo = $this->input->post('cardNo');
        	$addedDate = $this->input->post('redeemDate');
        	$where='1';
        	if($phoneNo)
        	{
           $where = "(customer.phone like '%$phoneNo%' OR customer.mobile like '%$phoneNo%')"; 
        	}        	
        	if($customerName)
        	{
           	$this->db->like($this->table2.'.customerName', $customerName);
        	}
        	if($cardNo)
        	{
           	$this->db->where($this->table3.'.cardId', $cardNo);
        	}
        	if($addedDate)
        	{
           	$addedDate = date('Y-m-d', strtotime($addedDate));
            $this->db->where($this->table1.'.redeemDate', $addedDate);
        	}
	   		
		    $this->db->select('redeem.*,customer.customerName,card.cardId,customer.phone,customer.mobile');
			$this->db->from('redeem');
			$this->db->join('customer','customer.ID = redeem.customerId','left');
			$this->db->join('card','card.ID = redeem.cardId');
			
			if($loginType!="admin") {
				$where = "$where and redeem.loginId='$loginId'";
			    $this->db->where($where);
			    $this->db->order_by('redeem.ID');
			}
			else {
				if($where!=1)
				{
					$this->db->where($where);
				}
				
				$this->db->order_by('redeem.ID');
			}			
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    }
	    
	    
	    	/* function getBranchData(){
	   		
	   		$loginId = $_SESSION['user_id']; //print_r($loginId);die;
	   		$loginType = $_SESSION['user_type']; //print_r($loginId);die;
	   		//$branchId = $data['branchId'];
	   		
	   		
	   		$customerName = $this->input->post('customerName');
        	$phone = $this->input->post('phone');
        	$branch = $this->input->post('branch');
	   		
	   		if($customerName)
        	{
           	$this->db->like($this->table2.'.customerName', $customerName);
        	}
        	if($phone)
        	{
           	$this->db->where($this->table2.'.phone', $phone);
        	}
        	
        	if($loginType=="admin" && $loginType!="branch")
        	{
				if($branch)
	        	{
	           	$this->db->like($this->table4.'.branchName', $branch);
	        	}
			}
	   		
		    $this->db->select('SUM(point_details.point) as total,customer.*,login.id as logId,branch.branchName');
			$this->db->from('point_details');
			$this->db->join('customer','customer.ID = point_details.customerId','left');
			$this->db->join('login','login.id = point_details.loginId','right');
			$this->db->join('branch','branch.ID = login.branchId');
			if($loginType!="admin") {
				$where = "point_details.loginId='$loginId'";
				$this->db->where($where);
				$this->db->order_by('point_details.ID');
			}
			else {
				$this->db->group_by('branch.ID');
			}	
				
			
			
			$query = $this->db->get();
			
			return $query->result();
	    }
	    
	    function getTotalPoints() {
			$this->db->select('SUM(point_details.point) as total');
			$this->db->from('point_details');
			$this->db->order_by('point_details.ID');
			$query = $this->db->get();
			
			$data = $query->row();
			if($data)
			{
				$result = $data->total;
			}
			else{
				$result = "0";
				}
			return $result;
		} */

            function getBranchSearchData($data){
	   		$cardId			=	$data['cardId'];
	        $customerName	=	$data['customerName'];
	        $phone			=	$data['phone'];
	        
	        
	         $this->db->select('SUM(point_details.point) as total,customer.customerName,branch.branchName,customer.*,card.id as cardID');
			$this->db->from('point_details');
			$this->db->join('card','card.ID = point_details.cardId');
			$this->db->join('customer','customer.ID = point_details.customerId');
			$this->db->join('login','login.id = point_details.loginId');
			$this->db->join('branch','branch.ID = login.branchId','right');
			
			if($customerName && $phone)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone) OR (customer.customerName='$customerName' AND customer.mobile=$phone))";
				
			}
			if($phone && !$customerName)
			{
				$where = "(customer.phone='$phone' OR customer.mobile='$phone')"; 
			}
			if($cardId)
			{
				$where = "(card.cardId=$cardId)";
			}
			if($customerName && $phone && $cardId)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone AND card.cardId=$cardId) OR (customer.customerName='$customerName' AND customer.mobile=$phone AND card.cardId=$cardId))";
			}
			if($cardId && $phone)
			{
				$where = "((card.cardId='$cardId' AND customer.phone=$phone) OR (card.cardId='$cardId' AND customer.mobile=$phone))";
			}
			
			
			$this->db->where($where);
			
			$this->db->group_by('branch.ID,point_details.customerId');	
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }
          
         //purchase return report
	
	public function getAllPurReData()
	{
        $loginType = $_SESSION['user_type'];
		$loginId = $_SESSION['user_id'];

        $customerName = $this->input->post('customerName');
    	$phone = $this->input->post('phone');
    	//$mobile1 = $this->input->post('mobile1');
    	//$mobile2 = $this->input->post('mobile2');
    	$cardNo  = $this->input->post('cardNo');
    	$where = '1';
    	if($customerName)
    	{
       	$this->db->where($this->table2.'.customerName', $customerName);
    	}
    	if($phone)
    	{
       	$where = "(customer.phone like'%$phone%' OR customer.mobile1 like '%$phone%' OR customer.mobile2 like '%$phone%')"; 
    	}
    	/*if($mobile1)
    	{
       	$this->db->where($this->table2.'.mobile1', $mobile1);
    	}
    	if($mobile2)
    	{
       	$this->db->where($this->table2.'.mobile2', $mobile2);
    	}*/
		if($cardNo)
    	{
       	$this->db->where($this->table3.'.ID', $cardNo);
    	}   
                    
		$this->db->select('card.*,customer.*,purchase_return.*,customer.ID as customerId,card.ID as card_id,card.cardId as cardNo,purchase_return.ID as purchase_return_id ');
		$this->db->from('card');
		$this->db->join('customer','card.customerId = customer.ID');
		$this->db->join('purchase_return','purchase_return.cardId = card.ID');
		
		if($loginType!="admin") {
			$where = "$where and card.loginId='$loginId'";
			$this->db->where($where);
			$this->db->order_by('purchase_return.ID');
		}
		else {
			if($where!=1)
				{
					$this->db->where($where);
				}
			$this->db->order_by('purchase_return.ID');
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	    
} 