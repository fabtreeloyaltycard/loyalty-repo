<?php

class Customer_report_model extends CI_Model{
	protected $table1 = 'customer';
	protected $table2 = 'point_details';
	protected $table3 = 'branch';
	protected $table4 = 'login';
	protected $table5 = 'card';
	protected $condition = '1';

	public function __construct(){
		parent::__construct();
                $this->load->model('Redeem_model');
		$this->load->database();
	}

	public function get_customer($branchId=NULL,$customerName=NULL,$phoneNo=NULL){
                $loginId = $_SESSION['user_id']; 
                $loginType = $_SESSION['user_type'];
                $where ='1';
		if($customerName)
		{
			$this->condition = $this->condition.' AND A.customerName like"%'.$customerName.'%"' ;
		}
		if($branchId)
		{
			$this->condition = $this->condition.' AND D.ID ="'.$branchId.'"' ;
		}
                if($phoneNo)
		{
			$where = "(A.phone='$phoneNo' OR A.mobile='$phoneNo')"; 
		}
              
              if($loginType!="admin") {     
		$query =$this->db->query('SELECT A.ID,A.prefix,A.customerName,A.address,A.phone,A.mobile,A.loginId,A.addedDate,D.branchName, SUM(B.point) as point,E.ID as cardTabId  FROM '.$this->table1.' A LEFT JOIN '.$this->table2.' B ON B.customerId = A.ID LEFT JOIN '.$this->table5.' E ON B.cardId=E.ID LEFT JOIN '.$this->table4.' C on C.ID=A.loginId LEFT JOIN '.$this->table3.' D on C.branchId=D.ID Where '.$this->condition.' AND '.$where.' AND A.loginId='.$loginId.' GROUP BY A.ID');
		//echo $this->db->last_query();
		return $query->result_array();
		
               }
               else {
                   $query =$this->db->query('SELECT A.ID,A.prefix,A.customerName,A.address,A.phone,A.mobile,A.loginId,A.addedDate,D.branchName, SUM(B.point) as point,E.ID as cardTabId FROM '.$this->table1.' A LEFT JOIN '.$this->table2.' B ON B.customerId = A.ID LEFT JOIN '.$this->table5.' E ON B.cardId=E.ID LEFT JOIN '.$this->table4.' C on C.ID=A.loginId LEFT JOIN '.$this->table3.' D on C.branchId=D.ID Where '.$this->condition.' AND '.$where.' GROUP BY A.ID');
                   //echo $this->db->last_query();
		return $query->result_array();
                }  

	}

	// 
	public function get_cards(){
		
		$this->db->select('point_details.*,card.cardId as cardNo,SUM(point_details.point) as points, card.ID as cardId');
		 $this->db->from('point_details');
		 $this->db->join('card','card.ID = point_details.cardId');
		 
		 $this->db->group_by('card.ID');
		
		 $query = $this->db->get();
		 //echo $this->db->last_query();
		
		//$query	=	$this->db->get($this->table2);
		return $query->result_array();
	}
	public function get_branch(){
		$query	= $this->db->get($this->table3);
		return $query->result_array();
	}

}