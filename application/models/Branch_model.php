<?php 
   class Branch_model extends CI_Model {
	protected $table='branch';
      function __construct() { 
         parent::__construct(); 
      } 
   	
   	public function insertData($params)
   	{ 
		$ins	=	$this->db->insert($this->table,$params);
		$lastInsertId = $this->db->insert_id();
		return $lastInsertId;
	}
	
	public function getData($params)
	{
		$this->db->select($params['fields']);
		$this->db->order_by($params['order']);
		$query=$this->db->get_where($this->table);//, $params['condition']
		return $query->result_array();
	}
     
     public function getUpdateData($params)
     { 
     	$this->db->select($params['fields']);
		$query=$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	 }
	 public function updateAction($params,$editId)
	 {
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up	=	$this->db->update($this->table,$params);	
		return $up;
	 }
	 //delete
	  public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
         
        //branch name is already exists 
        public function branch_exists($branchName) {
	  	$this->db->where('branchName',$branchName);
	  	$query = $this->db->get('branch'); //echo  $query;die;
	    $num = $query->num_rows(); //echo $num;die;
	    return $num;
	  } 

        //edit case branch name is already exists or not checking
	  public function editBranchCheck($branchName,$editId){//,$editId){
	  	$this->db->where('branchName',$branchName);
	  	$this->db->where('ID!='.$editId);
	  	$query = $this->db->get('branch'); 
	  	$num = $query->num_rows();
	  	return $num;
	  } 

   } 
