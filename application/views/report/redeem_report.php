<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Redeem Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Redeem Report</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                <div class="box-header clearfix">
                   <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/Report_controller/redeem_report">
                                <input class="form-control" name="customerName" placeholder="Customer Name" value="<?= (isset($_REQUEST['customerName']) && $_REQUEST['customerName']!= "") ? $_REQUEST['customerName'] : "" ?>" type="text">
                               
                                <input class="form-control" name="phoneNo" placeholder="Phone Name" value="<?= (isset($_REQUEST['phoneNo']) && $_REQUEST['phoneNo']!= "") ? $_REQUEST['phoneNo'] : "" ?>" type="text">

                                <input class="form-control" name="cardNo" placeholder="Card No" value="<?= (isset($_REQUEST['cardNo']) && $_REQUEST['cardNo']!= "") ? $_REQUEST['cardNo'] : "" ?>" type="text">
                                <input class="form-control datepicker" name="redeemDate" placeholder="Date" value="<?= (isset($_REQUEST['redeemDate']) && $_REQUEST['redeemDate']!= "") ? $_REQUEST['redeemDate'] : "" ?>" type="text">
                                <button class="btn btn-flat btn-success" name="submit" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                    </div>
                </div>

    <?php if(isset($_REQUEST['submit'])) { ?>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Customer Name</th>
                                <th>Customer Phone</th>
                                <th>Card No</th>
                                <th>Radeem Point</th>
                                <th>Radeem Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                <?php if(empty($results)) { ?>
                             <tr>
                             	<td colspan="6" align="center"><h4>No Data Found.!</h4></td>
                             </tr>
                            <?php } else {?>
                                
                                <?php $i = 1; 
                                
                               	foreach($results as $r) {  
                               
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->customerName; ?></td>
                                        <td><?php echo $r->phone; ?></td>
                                        <td><?php echo $r->cardId; ?></td>
                                        <td><?php echo $r->redeemPoint; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($r->redeemDate)); ?></td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                          <?php } ?>
                        </table>
                    </div>
                   <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>