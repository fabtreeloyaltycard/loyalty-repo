<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Branch Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Branch Report</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                
               <div class="box-header clearfix">
                   <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/Report_controller/branch_report">
                                <input class="form-control" name="customerName" placeholder="Customer Name" value="<?= (isset($_REQUEST['customerName']) && $_REQUEST['customerName']!= "") ? $_REQUEST['customerName'] : "" ?>" type="text">
                                <input class="form-control" name="phone" placeholder="Phone No" value="<?= (isset($_REQUEST['phone']) && $_REQUEST['phone']!= "") ? $_REQUEST['phone'] : "" ?>" type="text">
                                <?php if(@$_SESSION['user_type'] == "admin") { ?>
									<input class="form-control" name="branch" placeholder="Branch" value="<?= (isset($_REQUEST['branch']) && $_REQUEST['branch']!= "") ? $_REQUEST['branch'] : "" ?>" type="text">
								<?php } ?>
                                <button class="btn btn-flat btn-success" name="submit" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                    </div>
                </div>
                	
      <?php if(isset($_REQUEST['submit'])) { ?>       
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                 <?php if($_SESSION['user_type']=="admin") { ?>
                                <th>Branch</th>
                                <?php } ?>
                                <th>Customer Name</th>
                                <th>Phone</th> 
                                <th>Branch Total</th>
                                <th>Total Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                <?php $i = 1; 
                                
                               	foreach($results as $r) { 
                               	$branchTotal = $r->total;  
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <?php if($_SESSION['user_type']=="admin") { ?>
                                        <td><?php echo $r->branchName; ?></td>
                                        <?php } ?>
                                        <td><?php echo $r->customerName; ?></td>
                                        <td><?php echo $r->phone; ?></td>
                                        <td><?php echo round($branchTotal,5) ; ?></td>
                                        <td><?php $allTotal = $this->Report_model->getTotalPoints($r->total); 
                                        echo round($allTotal,5)
                                        ?></td>
                                       
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                        </table>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>