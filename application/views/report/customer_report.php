<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Customer Reports
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Customer</li>
		</ol>
	</section>
    
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-primary">
					<div class="box-header clearfix">
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/reports/customer">
                                <input class="form-control" name="customerName" placeholder="Customer Name" value="<?= (isset($_REQUEST['customerName']) && $_REQUEST['customerName']!= "") ? $_REQUEST['customerName'] : "" ?>" type="text">
                                <input class="form-control" name="phoneNo" placeholder="Phone No" value="<?= (isset($_REQUEST['phoneNo']) && $_REQUEST['phoneNo']!= "") ? $_REQUEST['phoneNo'] : "" ?>" type="text">

                                <select class="form-control" name="branchId">
                                	<option value="">Branch</option>
                                	<?php foreach($branchs as $branch){ ?>
                                	<option value="<?=$branch['ID']?>" <?php if(@$_REQUEST['branchId']==$branch['ID']) echo 'selected';  ?> ><?=$branch['branchName'];?></option>
                                	<?php } ?>
                                </select>
                                <button class="btn btn-flat btn-success" type="submit" name="submit"><i class="ion ion-search" ></i></button>
                            </form>
                        </div>

                    </div>
					<!-- /.box-header -->
                                    <?php if(isset($_REQUEST['submit'])) { ?>
					<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Customer</th>
									<th>Phone</th>
									<th>Mobile</th>
									<th>Added Date</th>
									<th>Branch</th>
									<th>Total Point</th>
									<th>Card Details</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(empty($record))
								{
								?>
								<tr>
									<td colspan="9" align="center">
										No data found
									</td>
								</tr>
								<?php
								}
								else
								{
								$i = 1; 
								foreach($record as $customer){
									 
									$date= $customer['addedDate'];
									$cusId = $customer['ID'];//echo $cusId;
									$cardId = $customer['cardTabId'];//echo $cardId;
									//$cardIdArray = 
									$point = $customer['point'];
									
									$redeemSum=$this->Redeem_model->redeemSum($cardId);
                                    //$purchaseReSum=$this->Redeem_model->purchaseReturnSum($cardId);
									//$total = ($point-$purchaseReSum)-$redeemSum;
									$total = $point-$redeemSum;
									$addedDate=date('d-m-Y', strtotime($date));
									?>
									<tr>
										<td><?php echo $i++; ?></td>
										<td><?php echo $customer['prefix'].'.'.$customer['customerName']; ?></td>
										<td><?php echo $customer['phone']; ?></td>
										<td><?php echo $customer['mobile']; ?></td>
										<td><?php echo  $addedDate;?></td>
										<td><?php echo $customer['branchName']; ?></td>
										<td><?= round($total,2);?></td>
										<td>
										<a href="#" data-toggle="modal" data-target="#myModal<?php echo $customer['ID']; ?>" class="btn btn-success btn-flat">View</a>
										<div id="myModal<?php echo $customer['ID']; ?>" class="modal fade" role="dialog">
											<div class="modal-dialog">
											    <!-- Modal content-->
												<div class="modal-content">
											    	<div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Card</h4>
											      </div>
											      <div class="modal-body">
											      	<div class="box-body table-responsive no-padding">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Sl No</th>
																	<th>Card Number</th>
																	<th>Point</th>
																</tr>
															</thead>
															<tbody>
															<?php $j = 1; 
																foreach($cards as $card){ 
																	 $cardID= $card['cardId'];
																	//echo  $cardID;die;
																	$date= $customer['addedDate'];
																	$addedDate=date('d-m-Y', strtotime($date));
																	if($customer['ID']==$card['customerId'])
																	{
																	?>
																<tr>
																	<td><?php echo $j++; ?></td>
																	<td><?php echo $card['cardNo']; ?></td>
																	<td><?php $pointSum=$this->Redeem_model->pointSum($cardID);
$redeemSum=$this->Redeem_model->redeemSum($cardID); 
//$purchaseReSum=$this->Redeem_model->purchaseReturnSum($cardID);
                           
//echo round((($pointSum-$purchaseReSum)-$redeemSum),2); 
 echo round(($pointSum-$redeemSum),2); ?>
	
</td>
																</tr>
																<?php
																	}
																 } ?>
															</tbody>
														</table>
											    	</div>
											      </div>
											    	<div class="modal-footer">
											        <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Close</button>
											      </div>
												
											  	</div>
											</div>
										</div>
										</td>
									</tr>
									<?php }} ?>
                              
							</tbody>
						</table>
					</div>
                                       <?php } ?>
				</div>
			</div>
		</div>
	</section>
</div>
