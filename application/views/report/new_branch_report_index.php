<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Branch Report
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-primary">                   
					<div class="box-body">
						<form action="<?php echo site_url(); ?>/Report_controller/search" method="post">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Card Number</label>
										<input type="text" class="form-control" placeholder="Card No." name="cardNo" id="cardNo" value="<?php echo @$_REQUEST['cardNo'] ?>">
									</div>
								</div>
								<!--<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Customer Name</label> 
										<input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName"  value="<?php echo @$_REQUEST['customerName'] ?>">
									</div>    
								</div> -->
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Contact No</label> 
										<input type="text" class="form-control" placeholder="Contact No" name="phone" id="phone" value="<?php echo @$_REQUEST['phone'] ?>">
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3"></div> 
							</div>
									<div class="form-group">
										<!--<label style="visibility: hidden;">Placeholder</label>-->
										<button type="submit" class="btn btn-primary btn-flat" name="submit">Search</button>
									</div>
									</form>
								</div>
								<div id="sample" class="row"></div> 								
							</div>
							
					
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
</div>
<script>
$(document).ready(function(){
	$(function(){
		$('form').on('submit',function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var url = $(this).attr('action');
			
			var cardNo = $("#cardNo").val();//alert(cardNo);
			var customerName = $("#customerName").val();//alert(customerName);
			var phone = $("#phone").val();//alert(phone);
			var mobile = $("#mobile").val();//alert(mobile1);
			//alert(url)
			
			if( $("#mobile").val()  || $("#phone").val() || $("#cardNo").val() || ($("#customerName").val() && $("#phone").val()) || ($("#customerName").val() && $("#mobile").val()))
			{
			$.ajax({
				type:'post',
				url:url,
				data:data,
				success:function(data)
				{
					$('#sample').html(data);
					$('input[name="amount"]').trigger('focus');
					$('input[name="cardRadio"]:last').prop('checked', true);
				}
			})
		 }
		 else {
		 	alert("Search with Customer name and Contact no OR Card No OR Contact No");
		 }
		}
		)
	});
	});
</script>