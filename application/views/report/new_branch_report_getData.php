<section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                
                <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Customer Name</th> 
                                <th>Branch Name</th> 
                                <th>Phone Number</th> 
                                <th>Branch Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            	<?php
                            	$i = 1; 
                                
                                if(count($results1)=='')
                                {
                                ?>		
                                <tr>
                                	<td colspan="5" align="center"><h4>No data found.!</h4></td>
                                </tr>
                                
                                <?php
                                }
                                else 
                                {
                               	foreach($results1 as $r) { 
                               	//$i++;
                               	$Total = $r->total; 
                                $cardID = $r->cardID; //echo $cardID;die;
                                $redeemSum=$this->Redeem_model->redeemSum($cardID);
                                //$purchaseReSum = $this->Redeem_model->purchaseReturnSum($cardID);
                                $branchTotal = $Total-$redeemSum;
                            	?>
                                    <tr>
                                    <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->customerName; ?></td>
                                        
                                        <td><?php echo $r->branchName; ?></td>
                                        <td><?php echo $r->phone; ?></td>
                                       
                                        <td><?php echo round($branchTotal,2); ?></td>
                                       
                                    </tr>
                                  
                              <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                 
                </div>
            </div>
        </div>
    </section>