<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Fabtree-Loyalty Card</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url(); ?>css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/iCheck/flat/blue.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= base_url(); ?>js/jquery-2.2.4.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= site_url(); ?>/Login/adminDashboard" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg" style="margin-top: -19px;"><img src="<?= base_url(); ?>img/logo.png"</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span><?= $_SESSION['username']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                    <a href="<?= base_url(); ?>index.php/login/changePasswordForm" class="btn btn-flat btn-default">Change Password</a>
                                
                            </li>
                            <li class="user-footer">
                            	<?php if($_SESSION['user_type']=="admin") { ?>
                                
                                    <a href="<?= base_url(); ?>index.php/Login/reset_password" class="btn btn-flat btn-default">Reset Password</a>
                                <?php } ?>
                            </li>
                            <li class="user-footer">
                                    <a href="<?= base_url(); ?>index.php/login/logout" class="btn btn-flat btn-default">
                                        <span class="hidden-xs">Logout</span>
                                    </a>
                            </li>
                            
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar" style="top: 60px;">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= base_url(); ?>img/default_user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?= $_SESSION['username']; ?></p>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                <a href="<?= base_url(); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
                </li>
                <?php
                if(@$_SESSION['user_type'] == "admin")
                {
				?>
					<li>
	                <a href="<?= base_url(); ?>index.php/branch_controller/">
	                    <i class="fa fa-file-image-o"></i> <span>Branch</span>
	                </a>
	                </li>
	                <li class="treeview {{ (Request::is('admin/game-categories') || Request::is('admin/games')) ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-send"></i>
                        <span>Settings</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>index.php/Sms_setting_controller/index"><i class="fa fa-envelope-o"></i>Sms setting</a></li>
                        <!--<li><a href="<?= base_url(); ?>index.php/Sms_controller/broadcast"><i class="fa fa-envelope"></i>Broadcast SMS</a></li>
                        <li><a href="<?= base_url(); ?>index.php/Sms_controller/special_index"><i class="fa fa-envelope-o"></i>Customized SMS</a></li>-->
                       <!-- <li><a href="<?= base_url(); ?>index.php/Sms_controller/sentDetails"><i class="fa fa-envelope"></i>SMS sent details</a></li>-->
                        <!--<li><a href="/admin/games"><i class="fa fa-circle-o"></i>SMS Report</a></li>-->
                    </ul>
                </li>
                <!--<li>
                <a href="<?php echo base_url(); ?>index.php/Point_setting_controller/index">
                    <i class="fa fa-money"></i> <span>Point setting</span>
                </a>
                </li>-->
                <li>
                <a href="<?php echo base_url(); ?>index.php/OfferController/index">
                    <i class="fa fa-star-half-empty"></i> <span>Offers</span>
                </a>
                </li>
                <li class="treeview {{ (Request::is('admin/game-categories') || Request::is('admin/games')) ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-send"></i>
                        <span>SMS</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= base_url(); ?>index.php/Sms_controller/broadcast"><i class="fa fa-envelope"></i>Broadcast SMS</a></li>
                        <li><a href="<?= base_url(); ?>index.php/Sms_controller/special_index"><i class="fa fa-envelope-o"></i>Customized SMS</a></li>
                    </ul>
                </li>
				<?php } 
                if(@$_SESSION['user_type'] == "branch")
                {
				?>
				<li>
                <a href="<?= base_url(); ?>index.php/Invoice_controller/">
                    <i class="fa fa-file-image-o"></i> <span>Invoice</span>
                </a>
                </li>
                <li>
                <a href="<?= base_url(); ?>index.php/Redeem_controller/">
                    <i class="fa fa-minus"></i> <span>Redeem</span>
                </a>
                </li>
				<li>
                <a href="<?= base_url(); ?>index.php/customer_controller/">
                    <i class="fa fa-user-plus"></i> <span>Customer Registration</span>
                </a>
                </li>
                <li class="treeview {{ (Request::is('admin/game-categories') || Request::is('admin/games')) ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-send"></i>
                        <span>SMS</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= base_url(); ?>index.php/Sms_controller/special_index"><i class="fa fa-envelope-o"></i>Customized SMS</a></li>
                        <li><a href="<?= base_url(); ?>index.php/Sms_controller/broadcast"><i class="fa fa-envelope"></i>Broadcast</a></li>
                    </ul>
                </li>

                <?php } 
                if(@$_SESSION['user_type'] == "branch" || @$_SESSION['user_type'] == "admin")
                {
				?>
                
                <li class="treeview {{ (Request::is('admin/game-categories') || Request::is('admin/games')) ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Reports</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                         <li><a href="<?php echo base_url(); ?>index.php/Invoice_controller/report"><i class="fa fa-circle-o"></i>Invoice Report</a></li>
                         <li><a href="<?php echo base_url(); ?>index.php/Report_controller/redeem_report"><i class="fa fa-circle-o"></i>Redeem Details</a></li>
                         <li><a href="<?php echo base_url(); ?>index.php/reports/customer"><i class="fa fa-circle-o"></i>Customer Report</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Report_controller/index"><i class="fa fa-circle-o"></i>Branch Report</a></li>
 							<li><a href="<?php echo base_url(); ?>index.php/OfferController/report_index"><i class="fa fa-circle-o"></i>Offer Details </a></li>
  							<li><a href="<?php echo base_url(); ?>index.php/Log_report/index"><i class="fa fa-circle-o"></i>Log Report</a></li>
                    </ul>
                </li>
                  
               <?php } ?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>