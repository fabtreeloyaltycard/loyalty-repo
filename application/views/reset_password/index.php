<script>
	function getUname()
	{
		var branchId = document.getElementById('branchList').value;//alert(branchId);
		var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("uDiv").innerHTML = xmlhttp.responseText;
            }//alert(responseText);
        };
        xmlhttp.open("GET", "<?php echo site_url(); ?>/Login/getAjax?b="+branchId, true);
        xmlhttp.send();
	}

</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Reset Password
        </h1>
    </section>
    <section class="content">
        <?php if (validation_errors()) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="status status-danger" data-role="auto-hide">
                        <?= validation_errors() ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($error)) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="status status-danger" data-role="auto-hide">
                        <?= $error ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $flash = $this->session->flashdata('flash');
        if (isset($flash) && $flash) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="status status-<?= $flash['type']; ?>" data-role="auto-hide">
                        <?= $flash['message']; ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Reset Password</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <form action="<?= base_url(); ?>index.php/login/changeBranchPassword" method="post">
                                	<div class="form-group has-feedback">
                                		<label for="password">Branch Name</label> &nbsp;&nbsp;<span class="text-danger">*</span>
                                		<select class="form-control" name="branchList" id="branchList" onchange="getUname();">
                                		<option value="">Select Branch</option>
                                		<?php foreach($records as $r) { ?>
											<option value="<?= $r->ID; ?>"><?= $r->branchName; ?></option>
										<?php } ?>
                                		
                                			
                                		</select>
                                	</div>
                                	<div class="form-group has-feedback" id="uDiv">
                                		
                                	</div> 
                                    <div class="form-group has-feedback">
                                        <label for="nPassword">New Password</label> &nbsp;&nbsp;<span class="text-danger">*</span>
                                        <input type="password" id="nPassword" class="form-control" name="nPassword"
                                               placeholder="New Password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="cPassword">Confirm Password</label> &nbsp;&nbsp;<span class="text-danger">*</span>
                                        <input type="password" id="cPassword" class="form-control" name="cPassword"
                                               placeholder="Confirm Password Password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div>
