<form action="<?php echo site_url(); ?>/Invoice_controller/add" method="post">
<div class="row table-responsive">
	<table align="center" class="customer_details table">
	<?php
	$i = 1; 
	$previd = 0;
	if(!empty($results1))
	{
	foreach($results1 as $r){ 
		if($previd!=$r->customerId)
		{
		$previd = $r->customerId;
		?>
        <style>
            table{
                padding: 20px 0px;
                display: block;
                border-top: 1px solid #ccc;
                width: 80%;
            }
            .customer_details tr{
                width: 100%;
                display: table;
                height: 35px;
            }
            .customer_details th{
                width: 33%;
                display: inline-block;
            }
            .table{
                width: 95%;
                margin-bottom: 0;
                padding-bottom: 0;
            }
            tbody{
                width: 100%;
                display: inline-block;
            }
            thead{
                width: 100%;
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details{
                width: 100%;
            }
            .card_details tr{
                width: 100%;
                display: inline-table;
            }
            .card_details th{
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details td{
            }
            .crd_id{
                width: 100%;
            }
            .pints{
                border-bottom: 1px solid #ccc;
            }
            .crd_details{
                font-size: 18px;
                background: #4c4c4c;
                color: #fff;
                margin: 0;
                padding: 8px 0px;
                padding-left: 20px;
                display: inline-block;
                width: 100%;
                height: 50px;
            }
            .crd_details h3{
                float: left;
                font-size: 18px;
                line-height: 33px;
                margin: 0;
            }
            .submit_points{
                float: right;
                margin-right: 35px;
            }
            form .container{
                width: 100%;
            }
        </style>
		<input type="hidden" name="customerId" value="<?php echo $r->customerId; ?>">
		<tr>
			<th>Customer Name</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->customerName?></th>
		</tr>
		<tr>
			<th>Phone</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->phone?></th>
		</tr>
		<tr>
			<th>Mobile</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->mobile?></th>
		</tr>
		<tr>
			<th >Amount</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>	
			<th><input type="text" name="amount" id="amount" onkeyup="calculatePoint(this.value)" onmouseout="calculatePoint(this.value)" autocomplete="off" data-role="restrict_neg"></th>
		</tr>
        <tr>
           <th>Point</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><input type="text" name="point" id="pointText" readonly=""></th>	
        </tr>
        <tr></tr>
        <tr>
        	<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
        	<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
        	
        </tr>
		
    </table>
    </div>
    <div class="container">
        <div class="crd_details">
			<h3>Card Details</h3>	
			<!--<div class="form-group submit_points">
	        	<label>Sms Arabic</label>
				<input type="radio" name="smstype" value="smsArabic" checked=""/> 
				&nbsp;&nbsp;&nbsp;&nbsp;	
				<label>Sms English</label>
				<input type="radio" name="smstype" value="smsEnglish" />	
			</div>-->
            <div class="form-group submit_points">
                <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
            </div>
		</div> 
        <table align="left" class="card_details table" width="80%">
		<thead>
            <tr class="col-lg-12">
                <th class="col-lg-4">Card Id</th>
                <th class="col-lg-4">Total point</th>
                <th class="col-lg-4">Redeemed Point</th>
            </tr>
		</thead>
        <tbody>
	<?php }?>
		<tr class="pints col-lg-12">			
			<td  class="col-lg-4"><input style="margin-right:15px;" type="radio" name="cardRadio" onclick="setCardId(this.value)" value="<?php echo $r->card_id;?>"><?php echo $r->cardId?></td>
			<!--<td class="col-lg-6"><?php 
			//$pointSum=round(($this->Invoice_model->pointSum($r->card_id)),2);
			//$redeemSum=round(($this->Invoice_model->redeemSum($r->card_id)),2);
			//$purReturnSum=round(($this->Invoice_model->purchaseReturnSum($r->card_id)),2);
			//$totPoint = ($pointSum-$purReturnSum)-$redeemSum;
			//echo $totPoint; ?></td>-->
			<td class="col-lg-4">
				<?php
					$pointSum=round(($this->Invoice_model->pointSum($r->card_id)),2);
					$totPoint = round(($pointSum),3);
					echo $totPoint;
				?>
				
			</td>
			<td class="col-lg-4">
				<?php
					$redeemSum=round(($this->Invoice_model->redeemSum($r->card_id)),2);
					$totRedeemedPoint = round($redeemSum,3);
					if(isset($totRedeemedPoint))
					{
						echo $totRedeemedPoint;
					}
					else
					{
						echo "0";
					}
					
				?>
			</td>
		</tr>
        
	<?php 
	}
	?>
    </tbody>
	<input type="hidden" name="cardId" id="cardId" value="<?php echo $r->card_id?>">
	<?php
}
else
{
	?>
	<tr>
        <th></th>
		<th>No data found</th>
	</tr>
	<?php
}
?>
</table>
</div>
</form>
<script>
function setCardId(id)
{
	document.getElementById('cardId').value=id;
}
</script>