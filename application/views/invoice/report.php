<script>
	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
	
</script>


<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Invoice Report
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Invoice Report</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
					<span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
				<div class="box box-primary">
					<div class="box-header clearfix">
                   <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/Invoice_controller/report">
                                <input class="form-control" name="customerName" placeholder="Customer Name" value="<?= (isset($_REQUEST['customerName']) && $_REQUEST['customerName']!= "") ? $_REQUEST['customerName'] : "" ?>" type="text">
                                <input class="form-control" name="phone" placeholder="Phone" value="<?= (isset($_REQUEST['phone']) && $_REQUEST['phone']!= "") ? $_REQUEST['phone'] : "" ?>" type="text">
                                <input class="form-control" name="cardNo" placeholder="Card No" value="<?= (isset($_REQUEST['cardNo']) && $_REQUEST['cardNo']!= "") ? $_REQUEST['cardNo'] : "" ?>" type="text">
                                
                                <input class="form-control datepicker" name="addedDate" placeholder="Date" value="<?= (isset($_REQUEST['addedDate']) && $_REQUEST['addedDate']!= "") ? $_REQUEST['addedDate'] : "" ?>" type="text">
                                <button class="btn btn-flat btn-success" name="submit" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                    </div>
                </div>        
                               <?php if(isset($_REQUEST['submit'])) {?>               
					<div class="box-body">
						<form action="<?php echo site_url(); ?>/Invoice_controller/searchReport" method="post">
													
					<div class="box-body table-responsive no-padding" id="reportDiv">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Customer Name</th>
                                <th>Phone</th>
                                <th>Card No</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <!--<th>Action</th>-->
                            </tr>
                            </thead>
                            <tbody>
                                
                                <?php if(empty($results)) { ?>
                             <tr>
                             	<td colspan="5" align="center"><h4>No Data Found. !</h4></td>
                             </tr>
                             <?php } else { ?>
                                
                                <?php $i = 1; 
                               	foreach($results as $r) {  
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->customerName; ?></td>
                                        <td><?php echo $r->phone; ?></td>
                                        <td><?php echo $r->cardNo; ?></td>
                                        <td><?php echo $r->amount; ?></td>
                                        <td><?= date('d-m-Y', strtotime($r->addedDate)); ?></td>
                                        <td><!--<a href="<?php echo site_url(); ?>/sms_setting_controller/edit_view/<?php echo $r->ID;?>" class="btn btn-facebook btn-flat">Edit</a>-->
                                            <!--<a href="<?php echo site_url(); ?>/Invoice_controller/delete/<?php echo $r->point_details_id;?>" class="btn btn-danger btn-flat"  onclick="return delete_type()">Delete-->
                                            </a>
                                        </td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                          <?php } ?>
                        </table>
                    </div>
						</form>
					</div>
                                       <?php } ?>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
</div>
