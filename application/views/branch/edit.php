<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Branch Registration
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Branch Registration</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <span style="color: #ff0000" id="user"><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Branch Registration</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form action="<?php echo  site_url(); ?>/Branch_controller/edit" method="post">
                    <?php 
                    foreach($results as $r){  										
					?>
					<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      	<div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Branch Name</label> <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Branch Name" name="branchName" id="branchName" required value="<?php echo $r['branchName'];?>">
                                    </div>
                               </div>
	                            <div class="col-lg-4 col-md-4 col-sm-4"> 
	                                <div class="form-group">
	                                    <label for="title">Phone</label> 
	                                    <input type="tel" pattern="^\d{4}([- ]*)\d{7}$" class="form-control" placeholder="04xx-xxxxxxx" name="phone" id="phone" value="<?php echo $r['phone'];?>">
	                                </div>  
	                           	</div>
                      	</div>
                        <div class="row">
                               	 	<div class="col-lg-4 col-md-4 col-sm-4"> 
	                                    <div class="form-group">
	                                        <label for="title">Mobile</label> 
	                                        <input type="tel" class="form-control" placeholder="Mobile" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" name="mobile" id="mobile" value="<?php echo $r['mobile'];?>" onfocus="clearbox('mob')>
	                                    </div>
                                    </div>   
                                    <div class="col-lg-4 col-md-4 col-sm-4" > 
	                                    <div class="form-group">   
	                                     <label for="title">Email<span class="text-danger">*</span></label> 
	                                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo $r['email'];?>" required>
	                                    </div>  
                                    </div>      
                        </div>
                        <div class="row">                                
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <label for="title">Address</label> 
                                        <textarea name="address" class="form-control" placeholder="Address"><?php echo $r['address'];?></textarea>
                                </div> 
                       </div>
                                 <!--end of row--> 
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                                    	</div>
                                 	</div>
                                 </div>
                                   <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>