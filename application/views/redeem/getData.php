
<style>
	.redeemButton {
	color: #fff;
    background-color: #286090;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
}
</style>
<form action="<?php echo site_url(); ?>/Redeem_controller/add" method="post">
<div class="row table-responsive">
	<table align="center" class="customer_details table">
	<?php
	$previd =$cardNum= 0;
	if(!empty($results1))
	{
	foreach($results1 as $r){ 
		$mobile = $r->mobile;
		if($previd!=$r->customerId)
		{
		$previd = $r->customerId;
		
		?>
        <style>
            table{
                padding: 20px 0px;
                display: block;
                border-top: 1px solid #ccc;
                width: 80%;
            }
            .customer_details tr{
                width: 100%;
                display: table;
                height: 35px;
            }
            .customer_details th{
                width: 33%;
                display: inline-block;
            }
            .table{
                width: 95%;
                margin-bottom: 0;
                padding-bottom: 0;
            }
            tbody{
                width: 100%;
                display: inline-block;
            }
            thead{
                width: 100%;
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details{
                width: 100%;
            }
            .card_details tr{
                width: 100%;
                display: inline-table;
            }
            .card_details th{
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details td{
            }
            .crd_id{
                width: 100%;
            }
            .pints{
                border-bottom: 1px solid #ccc;
            }
            .crd_details{
                font-size: 18px;
                background: #4c4c4c;
                color: #fff;
                margin: 0;
                padding: 8px 0px;
                padding-left: 20px;
                display: inline-block;
                width: 100%;
                height: 50px;
            }
            .crd_details h3{
                float: left;
                font-size: 18px;
                line-height: 33px;
                margin: 0;
            }
            .submit_points{
                float: right;
                margin-right: 35px;
            }
            form .container{
                width: 100%;
            }
        </style>
		<input type="hidden" name="customerId" value="<?php echo $r->customerId; ?>">
		<tr>
			<th>Customer Name</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->customerName; ?></th>
		</tr>
		<tr>
			<th>Phone</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->phone; ?></th>
		</tr>
		<tr>
			<th>Mobile</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->mobile; ?></th>
		</tr>
		<tr>
			<th >Total redeem point</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>	
			<th><input type="text" name="totalRedeemVal" id="totalRedeemVal"  readonly=""></th>
		</tr>
                <tr></tr>
        <tr>
        	<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
        	<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
        	
        </tr>
        
    </table>
    </div>
    <div class="container">
        <div class="crd_details" id="mainSaveDiv">
			<h3>Card Details</h3>
            <div class="form-group submit_points">
                <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
            </div>
		</div>
       
        
        <table align="left" class="card_details table" width="80%">
		<thead>
            <tr class="col-lg-12">
                <th class="col-lg-4">Card Id</th>
                <th class="col-lg-4">Total point</th>
                 <th class="col-lg-4">Redeem Point</th>
            </tr>
		</thead>
        <tbody>
	<?php }?>
		<tr class="pints col-lg-12">			
			<td class="col-lg-4"><?php echo $r->cardId?><input type="hidden" name="cardId<?php echo ++$cardNum;?>" id="cardId<?php echo $cardNum;?>" value="<?php echo $r->card_id?>"></td>
			<td class="col-lg-4">
			
			<?php 
			/*$pointSum=$this->Redeem_model->pointSum($r->card_id);
			$redeemSum=$this->Redeem_model->redeemSum($r->card_id);
			$purReturnSum = $this->Redeem_model->purchaseReturnSum($r->card_id);
			$totRedeemPoint = round((($pointSum-$redeemSum)-$purReturnSum),2);
			echo  $totRedeemPoint; */
			
			$pointSum=round(($this->Redeem_model->pointSum($r->card_id)),2);
			$redeemSum=round(($this->Redeem_model->redeemSum($r->card_id)),2);
			$totRedeemPoint = round(($pointSum-$redeemSum),3);
			echo  $totRedeemPoint;
			?>
			
			</td>
			<td class="col-lg-4"><input type="text" data-role="redeem_input" name="redeem<?php echo $cardNum;?>" id="redeem<?php echo $cardNum;?>" onkeyup="getTotalRedeemPoint()"></td>
		</tr>
        
	<?php 
	}
	?>
    </tbody>
	<input type="hidden" name="totalCardNo" value="<?php echo $cardNum;?>" id="totalCardNo">
	<?php
}
else
{
	?>
	<tr>
        <th></th>
		<th colspan="3">No data found</th>
	</tr>
	<?php
}
?>
</table>
</div>
</form>
