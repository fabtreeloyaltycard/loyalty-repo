<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Redeem Point
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<?php
		        if ($this->session->flashdata('flash')) {
		            ?>
		            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
		                <?= $this->session->flashdata('flash')['message']; ?>
		            </div>
		            <?php
		        }
		        ?>
				<div class="box box-primary">                    
					<div class="box-body">
						<form action="<?php echo site_url(); ?>/Redeem_controller/search" method="post">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Card Number</label>
										<input type="text" class="form-control" placeholder="Card No." name="cardNo" id="cardNo" value="<?php echo @$_REQUEST['cardNo'] ?>">
									</div>
								</div>
							
								<!--<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Customer Name</label> 
										<input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName"  value="<?php echo @$_REQUEST['customerName'] ?>">
									</div>    
								</div>--> 
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Contact No</label> 
										<input type="text" class="form-control" placeholder="Contact No" name="phone" id="phone" value="<?php echo @$_REQUEST['phone'] ?>">
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3"></div>
							</div>
							<div class="form-group">
								
								<button type="submit" class="btn btn-primary btn-flat" name="submit">Search</button>
							</div>
						</form>
					</div>								
				</div>
			<div id="sample" class="row"></div> 
						
		</div>
	</div>
  </section>
</div>
<script>
$(document).ready(function(){
	$(function(){
		$('form').on('submit',function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var url = $(this).attr('action'); 
			var cardNo = $('#cardNo').val();
			var cusName = $('#customerName').val();
			var phone = $('#phone').val();//alert(phone);
			var mobile1 = $('#mobile').val();//alert(mobile);
			
			if($("#phone").val() || $("#mobile").val() || $('#cardNo').val() || ($('#customerName').val() && $('#phone').val()) || ($('#customerName').val() && $('#mobile').val()))
			{
			//alert(url)
				$.ajax({
					type:'post',
					url:url,
					data:data,
					success:function(data)
					{
						$('#sample').html(data);
						$('input[name="amount"]').trigger('focus');
						$('input[name="cardRadio"]:last').prop('checked', true);
					}
				})
			}
			else
			{
				alert("Search with Customer Name and Contact No OR Card No OR Contact No");
			}
		}
		)
	})
});	
</script>
<script>
function getTotalRedeemPoint()
{ var totalRedeem=0;
	totalCardNo=document.getElementById('totalCardNo').value; 
	document.getElementById('totalRedeemVal').value=0;
	for(i=1;i<=totalCardNo;i++)
	{
		redeemString="redeem"+i;
		redeemPoint=document.getElementById(redeemString).value;
		if(redeemPoint!=0||redeemPoint!='')
		totalRedeem=parseInt(totalRedeem)+parseInt(redeemPoint);
	}
	document.getElementById('totalRedeemVal').value=totalRedeem;
}
</script>
<script>
	function calculatePoint(amount)
	{	
	 $.ajax({
		type:'post',
		url:"<?php echo site_url() ?>/Invoice_controller/calculatePoint",
		data:{"amount":amount
			},
			success:function(data)
			{
				//alert(data)
				$('#pointText').val(data);
			}
		
	 });
	}
</script>	

<!--<script>
function getSave() {
	otpDiv = document.getElementById('otpSubmit').value; //alert(otpDiv);
	
	if(otpDiv=="") {
		document.getElementById('otpSubmit').style.display='none';
		document.getElementById('mainSaveDiv').style.display='block';
	}
}	
	
function otpFun()
	{
	otpMob= $('input:hidden[name=otpMob]').val();//alert(otpMob);
	$.ajax({
		type:'post',
		url:"<?php echo site_url() ?>/Redeem_controller/otpGenerate",
		data: { "otp" :otpMob
        
     },
     
			success:function(result)
			{
				//taking the otp value from getData
			    $('#otpId').val(result);
				//console.log(result);
				//alert(result);
			}
		
	 });
	} 	
	
	
</script>-->