<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Log Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Log Report</li>
        </ol>
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/Log_report/index">
                                <input class="form-control" name="customerName" placeholder="Customer Name" value="<?= (isset($_REQUEST['customerName']) && $_REQUEST['customerName']!= "") ? $_REQUEST['customerName'] : "" ?>" type="text">
                                <input class="form-control datepicker" name="addedDate" placeholder="Date" value="<?= (isset($_REQUEST['addedDate']) && $_REQUEST['addedDate']!= "") ? $_REQUEST['addedDate'] : "" ?>" type="text">
                                <input class="form-control" name="cardNo" placeholder="Card No" value="<?= (isset($_REQUEST['cardNo']) && $_REQUEST['cardNo']!= "") ? $_REQUEST['cardNo'] : "" ?>" type="text">
                                <button class="btn btn-flat btn-success" name="submit" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                        </div>
                    </div>

<?php if(isset($_REQUEST['submit'])) { ?>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Card Number</th>
                                <th>Total Amounts</th>
                                <th>Total Points</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                 
                                <?php if(empty($records)) { ?>
                             <tr>
                             	<td colspan="6" align="center"><h5>No Data Found.!</h5></td>
                             </tr>
                            <?php } else {?>
                                
                                <?php $i = 1; 
                                
                               	foreach($records as $r) {  
                               
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->cardNo; ?></td>
                                        <td><?php echo $r->tot_amount; ?></td>
                                        <td><?php echo $r->tot_point; ?></td>
                                        <td><?php echo $r->customerName; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($r->addedDate)); ?></td>
                                        <td>
                                        	<a href="<?php echo site_url(); ?>/Log_report/delete/<?php echo $r->id; ?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
                                        </td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                           <?php } ?>
                        </table>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
	
</script>