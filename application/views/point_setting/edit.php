
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Point setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Point setting</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Point setting</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form  method="post" action="<?php echo site_url(); ?>/point_setting_controller/edit" onsubmit="return checknum()">
                    <?php 
                    foreach($results as $r){  					
			?>
			<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                
                                    <div class="form-group">
                                        <label for="title">Amount</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" placeholder="Amount" name="amount" id="amount" required value="<?php echo $r['amount'];?>" onfocus="clearbox('amountDiv')">
                                    </div>
                                    <div id="amountDiv" style="color: #FF0000;"></div>
                                    <div class="form-group">
                                        <label for="title">Point <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Point" name="point" id="point" required="" value="<?php echo $r['point'];?>" onfocus="clearbox('pointDiv')">
                                    </div>                                                                                          <div id="pointDiv" style="color: #FF0000;"></div>
                                 </div> 
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>
    
<script>

	
	function checknum(){
		
		flag=false;
		var amount= document.getElementById('amount').value;
		var point = document.getElementById('point').value;
		
		if(amount=="" || amount==0 || isNaN(amount) || amount<0)
		{																			///for phone
		document.getElementById('amountDiv').innerHTML="Enter valid amount.";
		flag=true;
		}
		if(point=="" || point==0 || isNaN(point) || point<0)
		{																			///for phone
		document.getElementById('pointDiv').innerHTML="Enter valid points.";
		flag=true;
		}
		
		if(flag==true)
		{
		return false;
		}
	}
	
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}

</script>
