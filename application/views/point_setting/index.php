<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Point Setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Point Setting</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
            <span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <!--<div class="box-header">
                        <h3 class="box-title">List</h3>
                        <span class="pull-right"><a href="<?php echo base_url(); ?>index.php/point_setting_controller/add_view" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>-->
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>AmountFrom</th>
                                <th>AmountTo</th>
                                <th>Percentage</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                <?php $i = 1; 
                                
                               	foreach($records as $r) {  
                               
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->amountFrom; ?></td>
                                        <td><?php echo $r->amountTo; ?></td>
                                        <td><?php echo $r->percentage; ?></td>
                                        <!--<td><a href="<?php echo site_url(); ?>/point_setting_controller/edit_view/<?php echo $r->ID;?>" class="btn btn-facebook btn-flat">Edit</a>                                           
                                            </a>
                                        </td>-->
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>