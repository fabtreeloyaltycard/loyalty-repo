<form action="<?php echo site_url(); ?>/Sms_controller/special_send" method="post">
<div class="row table-responsive">
	<table align="center" class="customer_details table">
	<?php
	$i = 1; 
	$previd = 0;
	if(!empty($results1))
	{
	foreach($results1 as $r){ 
		if($previd!=$r->customerId)
		{
		$previd = $r->customerId;
		?>
        <style>
            table{
                padding: 20px 0px;
                display: block;
                border-top: 1px solid #ccc;
                width: 80%;
            }
            .customer_details tr{
                width: 100%;
                display: table;
                height: 35px;
            }
            .customer_details th{
                width: 33%;
                display: inline-block;
            }
            .table{
                width: 95%;
                margin-bottom: 0;
                padding-bottom: 0;
            }
            tbody{
                width: 100%;
                display: inline-block;
            }
            thead{
                width: 100%;
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details{
                
            }
            .card_details tr{
                width: 100%;
                display: inline-table;
            }
            .card_details th{
                display: inline-block;
                border-bottom: 1px solid #ccc;
            }
            .card_details td{
            }
            .crd_id{
                width: 100%;
            }
            .pints{
                border-bottom: 1px solid #ccc;
            }
            h3.crd_details{
                font-size: 18px;
                background: #4c4c4c;
                color: #fff;
                margin: 0;
                padding: 8px 0px;
                padding-left: 20px
            }
            .submit_points{
                float: right;
                margin-top: 20px;
                margin-right: 35px;
            }
        </style>
		<input type="hidden" name="customerId" value="<?php echo $r->customerId; ?>">
		<input type="hidden" name="customerName" value="<?php echo $r->customerName; ?>">
		<input type="hidden" name="mobile" value="<?php echo $r->mobile; ?>">
		<tr>
			<th>Customer Name</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->customerName?></th>
		</tr>
		<tr>
			<th>Phone</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->phone?></th>
		</tr>
		<tr>
			<th>Mobile</th>
			<th>&nbsp;&nbsp;:&nbsp;&nbsp;</th>
			<th><?php echo $r->mobile?></th>
		</tr>
		<?php
		}
	}
	?>
	<div class="row">
	  <div class="col-lg-3 col-md-3 col-sm-3"></div>
	  <div class="col-lg-3 col-md-3 col-sm-3">
	       	<div class="form-group submit_points">
	       		<label>Special SMS</label>
	       		<textarea name="specialSms" placeholder="Enter msg here.!" style="width:400px;height:150px;"></textarea>
	        </div>
	   </div>
    </div>
<div class="row">
 	<div class="col-md-12 col-lg-12 col-sm-12">
 		<div class="form-group submit_points">
        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Send SMS</button>
    	</div>
 	</div>
 </div>
<?php	
}
if(empty($results1)) { ?>
	<div style="text-align: center">
		<h4>No data found.!</h4>
	</div>
<?php } ?>		
    </table>
    </div>



</form>
<script>
function setCardId(id)
{
	document.getElementById('cardId').value=id;
}
</script>