<script>
	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
	
</script>

<?php 
$type = $_SESSION['user_type'];
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           SMS Sent Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">SMS Sent Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
            	<span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <!--<div class="box-header">
                        <h3 class="box-title">Offers</h3>
                        <span class="pull-right"><a href="<?php echo site_url(); ?>/OfferController/add_view" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>-->
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>SMS</th>
                                <th>Sent Date</th>
                                <th>Sent Time</th>
                                <th>Sent Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            	<?php 
								if(empty($records))
								{
								?>
								<tr>
									<td colspan="6" align="center">
										No data found
									</td>
								</tr>
								<?php
								}
								else
								{
								$i = $this->uri->segment(3);
                                
                               	foreach($records as $r) {  
                               	$i++;	
                                $sendTypeId = $r->sentType;
                               	
                               	if($sendTypeId==1) {
									$sendType = "Card Registration Time";
								}
								else if($sendTypeId==2) 
								{
									$sendType = "Invoice add Time";
								}
								else if($sendTypeId==3)
								{
									$sendType = "Point redeem Time";
								}
								else if($sendTypeId==4)	
								{
									$sendType = "After job done";
								}
								else 
								{
									$sendType = "Broadcasting Time";
								}
                               ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $r->sms; ?></td>
                                        <td><?php echo $r->sentDate; ?></td>
                                        <td><?php echo $r->sentTime; ?></td>
                                        <td><?php echo $sendType; ?></td>
                                        <td>
                                        	<!--<a href="<?php echo site_url(); ?>/OfferController/edit_view/<?php echo $r->ID; ?>" class="btn btn-facebook btn-flat">Edit</a>-->
                                        	<?php if($type=="admin") { ?>
                                            <a href="<?php echo site_url(); ?>/Sms_controller/deleteSmsSent/<?php echo $r->ID; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat">Delete
                                            </a>
                                            <?php } else { ?>
                                            <a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Delete</a> 
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                            <?php } ?>
                        </table>
                    </div>
                  	<?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!--end -->
                </div>
            </div>
        </div>
    </section>
</div>