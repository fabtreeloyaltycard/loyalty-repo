<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Broadcast
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Broadcast</li>
		</ol>
		<?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
	</section>
    
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-primary">
					
					            <?php 
								  if(empty($records)) { ?>
								  <div class="row">
								   <h4 align="center">No Customer Here. !</h4>
								  </div>
								<?php
								  }
								  else {
								  	
								?>
								<?php 
								foreach($sendSms as $s){ 
                                                                 
									$sms= $s->sms;
									
									?>
									
							<div class="box-header">
							  <div class="row">							  	
	                       			<div class="col-lg-4 col-md-4 col-sm-4">
	                                    <div class="form-group">
	                                     <label for="title">Sms Content</label> 
	                                        <input type="text" class="form-control" value="<?php echo $sms; ?>" readonly=""> 
	                                    </div>
	                               	</div>
                               </div>
                               <div class="row">	
	                       			<div class="col-lg-4 col-md-4 col-sm-4">
	                                    <div class="form-group">
	                                        <label for="title">Total Customers</label> 
	                                        <input type="text" class="form-control" value="<?php echo count($records);?>" readonly="">
	                                    </div>
	                               	</div>
                               </div>
                               
                               <div class="row" >
                               	
                               	<div class="col-lg-4 col-md-4 col-sm-4">
                               		<?php
						$new=0; 
						$new =  count($records);//echo $new;die;
						foreach($records as $r) {
						$new++;
                                                }
						if($new!=0) {
						?>
						
				<form name="sendSms" method="post" action="<?php echo site_url(); ?>/Sms_controller/broadcastSms">			
					
                       		<!--<label>Sms Arabic</label>
                       		<input type="radio" name="smstype" value="smsArabic" checked=""/>
	                   
	                   		<label>Sms English</label>
	                   		<input type="radio" name="smstype" value="smsEnglish"/>-->
					
						<div class="row">
						  <p></p>
						  <div class="col-lg-4 col-md-4 col-sm-4">	
							<span class="pull-right"><button type="submit" class="btn btn-primary btn-flat" name="submit">Send SMS</button></span>
						  </div>	
                        </div>
                                               
                        <?php } ?>
						</form>
                               	
                               	</div>
                               </div>
                               
							</div>	
									
						<?php } }?>
					</div>
				</div>
                     
			</div>
		</div>
	</section>
</div>
