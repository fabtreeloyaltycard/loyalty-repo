<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Purchase Return entry
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<!--<li class="active">Invoice Entry</li>-->
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
					<span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
				<div class="box box-primary">
					<!--<div class="box-header">
						<h3 class="box-title">Invoice Entry</h3>
					</div>-->
					<!-- /.box-header -->                    
					<div class="box-body">
						<form action="<?php echo site_url(); ?>/Purchase_return_controller/search" method="post">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Card Number</label>
										<input type="text" class="form-control" placeholder="Card No." name="cardNo" id="cardNo" value="<?php echo @$_REQUEST['cardNo'] ?>">
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Customer Name</label> 
										<input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName"  value="<?php echo @$_REQUEST['customerName'] ?>">
									</div>    
								</div> 
								
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group">
										<label for="title">Contact No</label> 
										<input type="text" class="form-control" placeholder="Contact No" name="phone" id="phone" value="<?php echo @$_REQUEST['phone'] ?>">
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3"></div>
							</div>
									<div class="form-group">
										<!--<label style="visibility: hidden;">Placeholder</label>-->
										<button type="submit" class="btn btn-primary btn-flat" name="submit">Search</button>
									</div>
								</div>								
							</div>
							<div id="sample" class="row"></div> 
						</form>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
</div>
<script>
$(document).ready(function(){
	$(function(){
		$('form').on('submit',function(e){
			e.preventDefault();
			var data = $(this).serialize(); 
			var url = $(this).attr('action');
			
			var cardNo = $("#cardNo").val();//alert(cardNo);
			var customerName = $("#customerName").val();//alert(customerName);
			var phone = $("#phone").val();//alert(phone);
			var mobile1 = $("#mobile1").val();//alert(mobile1);
			var mobile2 = $("#mobile2").val();//alert(mobile2);
			//alert(url)
			
			if($("#phone").val() || $("#mobile1").val() || $("#mobile2").val() || $("#cardNo").val() || ($("#customerName").val() && $("#phone").val()) || ($("#customerName").val() && $("#mobile1").val()) || ($("#customerName").val() && $("#mobile2").val()))
			{
			$.ajax({
				type:'post',
				url:url,
				data:data,
				success:function(data)
				{
					$('#sample').html(data);
					$('input[name="amount"]').trigger('focus');
					$('input[name="cardRadio"]:last').prop('checked', true);
				}
			})
		 }
		 else {
		 	alert("Search with Customer Name and Contact No OR Card No OR Contact No");
		 }
		}
		)
	});
	});
</script>
<script>
function setCardId(id)
{
	alert(id);
	document.getElementById('cardId').value=id;
}
</script>
<script>
	function calculatePoint(amount)
	{	
	 $.ajax({
		type:'post'	 	,
		url:"<?php echo site_url() ?>/Purchase_return_controller/calculatePoint",
		data:{"amount":amount
			},
			success:function(data)
			{
				//alert(data)
				$('#pointText').val(data);
			}
		
	 });
	}
</script>	