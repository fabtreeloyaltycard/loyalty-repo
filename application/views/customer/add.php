<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer</li>
        </ol>
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Customer</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/Customer_controller/add" method="post" onsubmit="return valid();">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Prefix</label> <!--<span class="text-danger">*</span>-->
                                        <select name="prefix" id="prefix" class="form-control">
                                        	<option>Mr</option>
                                        	<option>Mrs</option>
                                        	<option>Ms</option>
                                        </select>
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Customer Name<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" required="" value="<?php if(isset($_SESSION['customerName'])){ echo $_SESSION['customerName'];}?>">
                                    </div>
                               </div>                               
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Phone<!--<span class="text-danger">*</span>--></label> 
                                        <input type="tel" pattern="^\d{4}([- ]*)\d{7}$" class="form-control" placeholder="04xx xxxxxxx" name="phone" id="phone" placeholder="04xx xxxxxxx" value="<?php if(isset($_SESSION['phone'])){ echo $_SESSION['phone'];}?>">
                                    </div> 
                                 </div>                              
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <div class="form-group">
                                        <label for="title">Mobile <span class="text-danger">*</span></label> 
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" class="form-control" placeholder="Mobile" name="mobile" id="mobile" required="">
                                    </div>
                                  </div>
	                               
                               	 <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">E-mail</label>
                                        <input type="email" class="form-control" placeholder="E-mail" name="email" id="email" value="<?php if(isset($_SESSION['email'])){ echo $_SESSION['email'];}?>">
                                    </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
                                    <div class="form-group">
                                        <label for="title">Add date</label>
                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate"   value="<?php echo date('d-m-Y') ?>">
                                    </div>                                                                     
                                 </div>
                             </div>
                             <div class="row">
                             	<div class="col-lg-4 col-md-4 col-sm-4"> 
	                                <div class="form-group">
	                                    <label for="title">Address</label>
	                                    <textarea name="address" class="form-control" rows="5"><?php if(isset($_SESSION['address'])){ echo $_SESSION['address'];}?></textarea>
	                                </div>  
	                           	 </div>
                             </div> 
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>