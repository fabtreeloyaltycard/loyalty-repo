<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Customer</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form  method="post" action="<?php echo site_url(); ?>/customer_controller/edit">
                    <?php 
                    foreach($results as $r){  										
					?>
					<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Prefix</label> <!--<span class="text-danger">*</span>-->
                                        <select name="prefix" id="prefix" class="form-control">
                                        	<option <?php if($r['prefix']=='Mr'){ echo 'selected'; } ?>>Mr</option>
                                        	<option <?php if($r['prefix']=='Mrs'){ echo 'selected'; } ?>>Mrs</option>
                                        	<option <?php if($r['prefix']=='Ms'){ echo 'selected'; } ?>>Ms</option>
                                        </select>
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Customer Name <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" required="" value="<?php echo $r['customerName'];?>">
                                    </div>
                               </div>                               
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Phone</label> 
                                        <input type="tel" pattern="^\d{4}([- ]*)\d{7}$" class="form-control" placeholder="04xx-xxxxxxx" name="phone" id="phone"  value="<?php echo $r['phone'];?>">
                                    </div> 
                                 </div>                              
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <div class="form-group">
                                        <label for="title">Mobile<span class="text-danger">*</span></label> 
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" class="form-control" placeholder="Mobile" name="mobile" id="mobile" required="" value="<?php echo $r['mobile'];?>">
                                    </div>
                                  </div>
                               	 <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">E-mail</label> 
                                        <input type="email" class="form-control" placeholder="E-mail" name="email" id="email" value="<?php echo $r['email'];?>">
                                    </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
                                    <div class="form-group">
                                        <label for="title">Add date</label> 
                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate"      value="<?php echo $r['addedDate'];?>">
                                    </div>                                                                     
                                 </div> 
                             </div><!--end of row--> 
                             <div class="row">
                             	<div class="col-lg-4 col-md-4 col-sm-4"> 
	                                <div class="form-group">
	                                    <label for="title">Address</label>
	                                    <textarea name="address" class="form-control" rows="5"><?= $r['address']; ?></textarea>
	                                </div>  
	                           	 </div>
                             </div>   
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>