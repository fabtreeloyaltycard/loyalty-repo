<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
	</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Customer
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Customer</li>
		</ol>
		<?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
	</section>
    
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">List</h3>
						<span class="pull-right"><a href="<?php echo site_url(); ?>/customer_controller/add_view" class="btn btn-primary btn-flat">Add New</a></span>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Customer</th>
									<th>Phone</th>
									<th>Mobile</th>
									<th>Add Date</th>
									<th>Card Details</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(count($records)=="")
									{
								?>
									<tr>
										<td colspan="7" align="center"><h4>No data found.!</h4></td>
									</tr>
								<?php		
									}
									else 
									{
								?>
								<?php $i = $this->uri->segment(3); 
								foreach($records as $r){
                                    $flag = 0;
                                    $i++;  
									$date= $r->addedDate;
                                    $loginId= $r->loginId;
									$addedDate=date('d-m-Y', strtotime($date));
                                    $logID = $_SESSION['user_id'];//echo $logID;die;
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $r->prefix.". ".$r->customerName; ?></td>
										<td><?php echo $r->phone; ?></td>
										<td><?php echo $r->mobile; ?></td>
										<td><?php echo  $addedDate;?></td>
										<td>
										<?php 
										foreach($cardRecords as $card){  
											if($r->ID==$card->customerId)
											{
											 //$flag = 1;
											 ?><li><?php echo  $card->cardId."\r\n";?></li><?php
											 }
										}?>
										</td>
										<td>
  <?php if($loginId==$logID) { ?>
<a href="<?php echo site_url(); ?>/customer_controller/edit_view/<?php echo $r->ID;?>" class="btn btn-facebook btn-flat">Edit</a>
											<a href="<?php echo site_url(); ?>/customer_controller/delete/<?php echo $r->ID;?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
											</a>
											<?php } ?>
											
											<a href="#" data-toggle="modal" data-target="#myModal<?php echo $r->ID; ?>" class="btn btn-success btn-flat">Add Card</a>
											
											<?php //if($flag == 1) { ?>
											<!--<a href="#" data-toggle="modal" data-target="#myModal2<?php echo $r->ID; ?>" class="btn btn-success btn-flat">Edit Card</a>-->
											<?php //} ?> 
									
											
										
											
											
											
											<!-- Modal -->
											<div id="myModal<?php echo $r->ID; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <form action="<?php echo site_url();?>/Card_controller/add_card" method="post">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Add Card</h4>
											      </div>
											      
											      <div class="modal-body">
											        <!--<p>Some text in the modal.</p>-->
											        <input type="hidden" name="customerId" value="<?php echo $r->ID; ?>">
											        <input type="hidden" name="branchId" value="1">
											        <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
				                                    <div class="form-group">
				                                        <label for="title">Add date<span class="text-danger">*</span></label> 
				                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate" required="" value="<?php echo date('d-m-Y') ?>">
				                                    </div>                                                                     
				                                 </div> 
											        <div class="row">                               
						                               <div class="col-lg-4 col-md-4 col-sm-4">
						                                    <div class="form-group">
						                                        <label for="title">Card No<span class="text-danger">*</span></label> 
						                                        <input type="number" class="form-control" placeholder="Card No" name="cardId" id="cardId" required="" min="0">
						                                    </div> 
						                               </div> 
                                                                               <div class="col-lg-4 col-md-4 col-sm-4">
							                               	<!--<div class="form-group">
							                               		<label>Sms Arabic</label>
							                               		<input type="radio" name="smstype" value="smsArabic" checked=""/>
							                               	</div>
							                               	<div class="form-group">
							                               		<label>Sms English</label>
							                               		<input type="radio" name="smstype" value="smsEnglish"/>
							                                </div>-->
						                               </div>
						                           </div>
						                         
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
											      </div>
											      
											      <div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Card Number</th>	
									<th>Added Date</th>								
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php $j = 1; 
								foreach($cardRecords as $card){  
									$date= $r->addedDate;
									$addedDate=date('d-m-Y', strtotime($date));
									if($r->ID==$card->customerId)
									{
									?>
								<tr>
									<td><?php echo $j++; ?></td>
									<td><?php echo $card->cardId; ?></td>
									<td><?php echo $card->addedDate; ?></td>
									<td><a href="<?php echo site_url(); ?>/card_controller/delete/<?php echo $card->ID;?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
											</a>
									</td>
								</tr>
								<?php
									}
								 } ?>
							</tbody>
						</table>
											    </div>
												</form>
											  </div>
											</div>
										</td>
										<td>
											<!-- Modal2 -->
											<div id="myModal2<?php echo $r->ID; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <form action="<?php echo site_url();?>/Card_controller/edit_card" method="post">
											   <!--<input type="hidden" value="<?php echo $card->ID ?>" name="editId">-->
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Edit Card</h4>
											      </div>
											      
											      <div class="modal-body">
											        <!--<p>Some text in the modal.</p>-->
											        <input type="hidden" name="customerId" value="<?php echo $r->ID; ?>">
											        <input type="hidden" name="branchId" value="1">
											        <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
				                                    <div class="form-group">
				                                        <label for="title">Add date<span class="text-danger">*</span></label> 
				                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate" required="" value="<?php echo date('d-m-Y') ?>">
				                                    </div>                                                                     
				                                 </div> 
											        <div class="row">                               
						                               <div class="col-lg-4 col-md-4 col-sm-4">
						                                    <div class="form-group">
						                                        <label for="title">New Card No<span class="text-danger">*</span></label> 
						                                        <input type="text" class="form-control" placeholder="Card No" name="cardId" id="cardId" required="">
						                                    </div> 
						                               </div> 
                                                                               <div class="col-lg-4 col-md-4 col-sm-4">
							                               	<div class="form-group">
							                               		<label>Sms Arabic</label>
							                               		<input type="radio" name="smstype" value="smsArabic" checked=""/>
							                               	</div>
							                               	<div class="form-group">
							                               		<label>Sms English</label>
							                               		<input type="radio" name="smstype" value="smsEnglish"/>
							                                </div>
						                               </div>
						                           </div>
						                         
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
											      </div>
											      
						<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th></th>
									<th>Sl No</th>
									<th>Card Number</th>	
									<th>Added Date</th>		
								</tr>
							</thead>
							<tbody>
							<?php $j = 1; 
								$firstTime = true;
								foreach($cardRecords as $card){  
									
									$date= $r->addedDate;
									$addedDate=date('d-m-Y', strtotime($date));
									if($r->ID==$card->customerId)
									{
									?>
								<tr>
									<td><input type="radio" value="<?php echo $card->ID; ?>" name="radio" checked=""/></td>
									<td><?php echo $j++; ?></td>
									<td><?php echo $card->cardId; ?></td>
									<td><?php echo $card->addedDate; ?></td>
									<!--<td><a href="<?php echo site_url(); ?>/card_controller/delete/<?php echo $card->ID;?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
											</a>
									</td>-->
								</tr>
								<?php
									}
								 } ?>
							</tbody>
						</table>
											    </div>
												</form>
											  </div>
											</div>
										</td>
									</tr>
									<?php } } ?>
							</tbody>
						</table>
					</div>
				</div>
                              <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!--end -->
			</div>
		</div>
	</section>
</div>
