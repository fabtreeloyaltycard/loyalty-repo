<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Add Card
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Customer</li>
		</ol>
	</section>
    
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">List</h3>
						<span class="pull-right"><a href="<?php echo base_url(); ?>index.php/customer_controller/add_view" class="btn btn-primary btn-flat">Add New</a></span>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Customer</th>
									<th>Place</th>
									<th>Phone</th>
									<th>Mobile</th>
									<th>Add Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php $i = 1;                                 
								foreach($records as $r){  
									$date= $r->addedDate;
									$addedDate=date('d-m-Y', strtotime($date));
									?>
									<tr>
										<td><?php echo $i++; ?></td>
										<td><?php echo $r->customerName; ?></td>
										<td><?php echo $r->place; ?></td>
										<td><?php echo $r->phone; ?></td>
										<td><?php echo $r->mobile1; ?></td>
										<td><?php echo  $addedDate;?></td>
										<td><a href="<?php echo base_url(); ?>index.php/customer_controller/edit_view/<?php echo $r->ID;?>" class="btn btn-facebook btn-flat">Edit</a>
											<a href="<?php echo base_url(); ?>index.php/customer_controller/delete/<?php echo $r->ID;?>" class="btn btn-danger btn-flat" >Delete
											</a>
											
											
											<a href="#" data-toggle="modal" data-target="#myModal<?php echo $r->ID; ?>" class="btn btn-success btn-flat">Add Card</a>
											<!-- Modal -->
											<div id="myModal<?php echo $r->ID; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <form action="<?php echo site_url();?>/Card_controller/add_card" method="post">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Add Card</h4>
											      </div>
											      
											      <div class="modal-body">
											        <!--<p>Some text in the modal.</p>-->
											        <input type="hidden" name="customerId" value="<?php echo $r->ID; ?>">
											        <input type="hidden" name="branchId" value="1">
											        <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
				                                    <div class="form-group">
				                                        <label for="title">Add date<span class="text-danger">*</span></label> 
				                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate" required="" value="<?php echo date('d-m-Y') ?>">
				                                    </div>                                                                     
				                                 </div> 
											        <div class="row">                               
						                               <div class="col-lg-4 col-md-4 col-sm-4">
						                                    <div class="form-group">
						                                        <label for="title">Card ID<span class="text-danger">*</span></label> 
						                                        <input type="text" class="form-control" placeholder="Card No" name="cardId" id="cardId" required="">
						                                    </div> 
						                               </div> 
						                           </div>
						                         
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
											      </div>
											    </div>
												</form>
											  </div>
											</div>
											 
										</td>
									</tr>
									<?php }?>
                              
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
