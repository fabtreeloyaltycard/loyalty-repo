<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Customer</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form  method="post" action="<?php echo site_url(); ?>/customer_controller/edit">
                    <?php 
                    foreach($results as $r){  										
					?>
					<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Title</label> <span class="text-danger">*</span>
                                        <select name="title" id="title" class="form-control">
                                        	<option <?php if($r['title']=='Dr'){ echo 'selected'; } ?>>Dr</option>
                                        	<option <?php if($r['title']=='Mr'){ echo 'selected'; } ?>>Mr</option>
                                        	<option <?php if($r['title']=='Mrs'){ echo 'selected'; } ?>>Mrs</option>
                                        	<option <?php if($r['title']=='Ms'){ echo 'selected'; } ?>>Ms</option>
                                        </select>
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Customer Name <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" required="" value="<?php echo $r['customerName'];?>">
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                     <div class="form-group">
                                        <label for="title">File Number<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="File Number" name="fileNumber" id="fileNumber" required="" value="<?php echo $r['fileNumber'];?>">
                                    </div> 
                               </div>
                              </div>
                              <div class="row">                               
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Phone<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" required="" value="<?php echo $r['phone'];?>">
                                    </div> 
                                 </div>                              
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <div class="form-group">
                                        <label for="title">Mobile 1<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Mobile 1" name="mobile1" id="mobile1" required="" value="<?php echo $r['mobile1'];?>">
                                    </div>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4">    
                                    <div class="form-group">
                                        <label for="title">Mobile 2<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Mobile 2" name="mobile2" id="mobile2" required="" value="<?php echo $r['mobile2'];?>">
                                    </div> 
                                </div>
                               </div>
                              <div class="row">
                                   <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">Place<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Place" name="place" id="place" required="" value="<?php echo $r['place'];?>">
                                    </div>  
                               		</div>
                               	 <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">E-mail<span class="text-danger">*</span></label> 
                                        <input type="email" class="form-control" placeholder="E-mail" name="email" id="email" required="" value="<?php echo $r['email'];?>">
                                    </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4" style="display: none">    
                                    <div class="form-group">
                                        <label for="title">Add date <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Add Date" name="addedDate" id="addedDate" required="" value="<?php echo $r['addedDate'];?>">
                                    </div>                                                                     
                                 </div> 
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>