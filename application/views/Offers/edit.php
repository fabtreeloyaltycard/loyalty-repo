<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Offer adding details
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Offer Adding</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Offer Adding Details</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form  method="post" action="<?php echo site_url(); ?>/OfferController/edit">
                    <?php 
                    foreach($results as $r){  	
                    $date= $r['offerSetDate'];
                    $toDate= $r['offerToDate'];
                    $setDate=date('d-m-Y', strtotime($date));									
                    $setToDate=date('d-m-Y', strtotime($toDate));									
					?>
			<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                
                                    <div class="form-group">
                                        <label for="title">Offer  <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Offer" name="offer" id="offer" required value="<?php echo $r['offer'];?>">
                                    </div>
                             </div>
                             <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Offer Description</label>
                                        <textarea name="offerDescription" id="offerDescription" placeholder="Offer Description" class="form-control"><?php echo $r['offerDescription'];?></textarea>
                                    </div>
                             </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-4 col-md-4 col-sm-6">  
                                    <div class="form-group">
                                        <label for="title">From Date  <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Set Date" name="offerSetDate" id="offerSetDate" required="" value="<?php echo $setDate; ?>">
                                    </div>  
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">  
                                    <div class="form-group">
                                        <label for="title">To Date  <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Set Date" name="offerToDate" id="offerToDate" required="" value="<?php echo $setToDate; ?>">
                                    </div>  
                            </div> 
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>