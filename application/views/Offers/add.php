<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Add Offers
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Offers</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <span style="color: #ff0000;height:20px; width:200px;" ><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Offers</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/OfferController/add" method="post">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Offer <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Offers" name="offer" id="offer" required>
                                    </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Offer Description</label> 
                                        <textarea name="offerDescription" class="form-control" placeholder="Offer Description"></textarea>
                                    </div>
                            </div>
                     </div>
                     <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">From Date <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Date" name="offerSetDate" id="offerSetDate" required="">
                                    </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">To Date <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="To Date" name="offerToDate" id="offerToDate" required="">
                                    </div>
                            </div>
                                   
                               </div> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
 </div>   
    