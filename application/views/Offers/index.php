<script>
	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
	
</script>



<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Offer Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Offer Details</li>
        </ol>
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
            	<span style="color: #ff0000"><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Offers</h3>
                        <span class="pull-right"><a href="<?php echo site_url(); ?>/OfferController/add_view" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Offer</th>
                                <th>Offer Description</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                <?php 
                                
                                if(count($records)=='')
                                {
                                ?>    
                                    <tr>
                                        <td colspan="5" align="center"><h4>No Data Found..!</h4></td>
                                    </tr>
                                <?php
                                }
                                else
                                {
                                $i = $this->uri->segment(3);
                                
                               	foreach($records as $r) {  
                                $i++;
                               ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $r->offer; ?></td>
                                        <td><?php echo $r->offerDescription; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($r->offerSetDate)); ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($r->offerToDate)); ?></td>
                                        <td>
                                        	<a href="<?php echo site_url(); ?>/OfferController/edit_view/<?php echo $r->ID; ?>" class="btn btn-facebook btn-flat">Edit</a>
                                            <a href="<?php echo site_url(); ?>/OfferController/delete/<?php echo $r->ID; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat">Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <?php  } } ?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
               <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!--end -->
            </div>
        </div>
    </section>
</div>