
<style>
 .typeClass {
 	 margin-top:5px;
 	 font-size: 13px;
 	 font-weight: bold;
 	 font-style: italic;
 	 color: #8c0000;
 }
.typeClass2 {
	 margin-top:5px;
 	 font-size: 13px;
 	 font-weight: bold;
 	 font-style: italic;
 	 color: #8c0000;
 	 text-align: right;
}
select {

    -webkit-appearance: none;
    -moz-appearance: none;
    text-indent: 1px;
    text-overflow: '';
}
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Sms setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Sms setting</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Sms setting</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form  method="post" action="<?php echo site_url(); ?>/sms_setting_controller/edit" onsubmit="return valid();">
                    <?php 
                    foreach($results as $r){  	
                    $date= $r['setDate'];
                    $setDate=date('d-m-Y', strtotime($date));									
					?>
			<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Sms Type</label> <span class="text-danger">*</span>
                                        <select class="form-control" disabled>
                                        	<option> Select</option>
                                        	<option value="1" <?php if($r['type']=='1'){ echo 'selected'; }?>>Card Registration Time</option>
                                        	<option value="2" <?php if($r['type']=='2'){ echo 'selected'; }?>>Invoice add Time</option>
                                        	<option value="3" <?php if($r['type']=='3'){ echo 'selected'; }?>>Point redeem Time</option>
                                        	<option value="5" <?php if($r['type']=='5'){ echo 'selected'; }?>>Broadcasting Time</option>
                                              
                                        </select>   
<input type="hidden" name="type" value="<?php echo $r['type']?>">                                    
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">    
                                    <div class="form-group">
                                        <label for="title">Set date <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Set Date" name="setDate" id="setDate" required="" value="<?php echo $setDate; ?>" onfocus="clearbox('dateDiv')" >
                                    </div>
                                    <div  id="dateDiv" style="height:20px; width:200px;color:#FF0000;"></div>                      
                                </div>
                                  
                        </div>
                        <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="title">SMS Content<span class="text-danger">*</span></label> 
                                        <textarea class="form-control" placeholder="Sms (English)" name="sms" id="sms" required="" rows="7" maxlength="160"><?php echo $r['sms'];?></textarea>
                                       <?php if($r['type']=='1') { ?>
									   	<div class="typeClass"> $ - means 'Card Number' </div>
									   <?php } if($r['type']=='2') { ?>
									   	 <div class="typeClass"> 
										   	 & - means 'Customer Name' <br />
										   	 @ - means 'Points' <br />
										   	 $ - means 'Card Number'
									   	 </div>	
									   <?php } if($r['type']=='3') { ?>
									   		<div class="typeClass"> 
										   	 @ - means 'Points' <br/>
										   	 $ - means 'Card Number'
									   	 	</div>	
									   <?php } if($r['type']=='6') { ?>
									   		<div class="typeClass"> 
										   	 @ - means 'Points' <br/>
										   	 $ - means 'Card Number'
									   	 	</div>	
									   	
									   <?php } ?>
                                    </div>
                                </div>
                                 
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>
    
    <script>
    	function valid(){
    		flag=false;
    		var date = document.getElementById('setDate').value;
    		
			regDate=/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/   //dob  dd/mm/yyyy
			if(!date.match(regDate))
			{		 
			document.getElementById('dateDiv').innerHTML="Enter date like dd/mm/yyyy";
			flag=true;
			}
			if(flag==true)
			{
			return false;
			}
		}
		
		//clear the validation msg
		function clearbox(Element_id)
		{
		document.getElementById(Element_id).innerHTML="";
		}
    </script>
    