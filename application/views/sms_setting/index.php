<div class="content-wrapper">
    <section class="content-header">
        <h1>
            SMS Setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">SMS Setting</li>
        </ol>
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List</h3>
                        <!--<span class="pull-right"><a href="<?php echo site_url(); ?>/sms_setting_controller/add_view" class="btn btn-primary btn-flat">Add New</a></span>-->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>SMS Type</th>
                                <th>SMS Content</th>
                                <th>Set date</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                <?php $i = 1; 
                                  $type='';
               function clean1($string) {
			   $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.

			   return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
			}  

			function clean2($string) {
			   $string1 = str_replace('A', ' ', $string); // Replaces all spaces with hyphens.
			   $string2 = str_replace('B', ' ', $string1); // Replaces all spaces with hyphens.
			   $string3 = str_replace('C', ' ', $string2); // Replaces all spaces with hyphens.
			   return  $string3;	
			}  

                               	foreach($records as $r) {  
                                $date= $r->setDate;
                                $SMSeng = $r->sms;
                                $setDate=date('d-m-Y', strtotime($date));
                                
                                if($r->type==1)
                                $type="Card Registration Time";
                                elseif($r->type==2)
                                $type="Invoice add Time";
                                elseif($r->type==3)
                                $type="Point redeem Time";
                                elseif($r->type==5)
                                $type="Broadcasting Time";
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?= $type; ?></td>
                                        <td><?php echo clean1($SMSeng); ?></td>
                                        <td><?php echo  $setDate;?></td>
                                        <td><a href="<?php echo site_url(); ?>/sms_setting_controller/edit_view/<?php echo $r->ID;?>" class="btn btn-facebook btn-flat">Edit</a>
                                        </td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>