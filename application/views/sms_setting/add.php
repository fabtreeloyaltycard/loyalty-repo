<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Sms setting
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>/Login/adminDashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Sms setting</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Sms setting</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/sms_setting_controller/add" method="post">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                
                                    <div class="form-group">
                                        <label for="title">Sms type</label> <span class="text-danger">*</span>
                                        <select name="type" id="type" required class="form-control">
                                        	<option> Select</option>
                                        	<option value="1">Card Registration Time</option>
                                        	<option value="2">Invoice add Time</option>
                                        	<option value="3">Point redeem Time</option>
                                        	<option value="4">After job done</option>
                                        	<option value="5">Broadcasting Time</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Sms <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="Sms" name="sms" id="sms" required="">
                                    </div>  
                                    <div class="form-group">
                                        <label for="title">Set date <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="Set Date" name="setDate" id="setDate" required="" value="<?php echo date('d-m-Y') ?>">
                                    </div>                                                                     
                                 </div> 
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>