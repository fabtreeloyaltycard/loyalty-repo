<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
    	<?php if($_SESSION['user_type']=="admin") { ?>
			
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('branch');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Branch</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-image-o"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Branch_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('sms_setting');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>

                        <p>SMS setting</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-send"></i>
                    </div>
                    <a href="<?php echo  base_url(); ?>/index.php/Sms_setting_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                     <div class="inner">
                        <p>Broadcast SMS</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-send"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>/index.php/Sms_controller/broadcast" class="small-box-footer" style="margin-top:45px;">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('offer_details');
							$query = $this->db->get();
							$rowcount = $query->num_rows();  
                    	?>
                        <h3><?= $rowcount ?></h3>

                        <p>Offer Setting</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-star-half-empty"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>/index.php/OfferController/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        <!-- branch loginTyppe------------->
        
        <?php if($_SESSION['user_type']=="branch") { ?> 
        
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$loginId = $_SESSION['user_id'];
                    		$this->db->from('point_details');
                    		$this->db->where('point_details.loginId='.$loginId);
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Invoice</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-image"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Invoice_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    	    $loginId = $_SESSION['user_id'];
                    		$this->db->from('redeem');
                    		$this->db->where('redeem.loginId='.$loginId);
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>

                        <p>Radeem</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-minus"></i>
                    </div>
                    <a href="<?php echo  base_url(); ?>/index.php/Redeem_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$loginId = $_SESSION['user_id'];
                    		$this->db->from('customer');
                    		$this->db->where('customer.loginId='.$loginId);
							$query = $this->db->get();
							$rowcount = $query->num_rows();  
                    	?>
                        <h3><?= $rowcount; ?></h3>

                        <p>Customer</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>/index.php/Customer_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<!--<?php
                    		$this->db->from('customer');
							$query = $this->db->get();
							$rowcount = $query->num_rows();  
                    	?>-->
                        <h3 style="visibility: hidden;">5</h3>
                       
                        <p>SMS</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-send"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>/index.php/Sms_controller/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <?php } ?>
    </section>
</div>
<!-- /.content-wrapper -->
