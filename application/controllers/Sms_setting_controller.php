<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Sms_setting_controller extends CI_Controller {
	  protected $baseFolder		=	'sms_setting';
	  protected $table			=	'sms_setting';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
	  
      public function __construct() { 
         parent::__construct();
        
		 // load 'session' 
		$this->load->library('session');
		 
		//including model
    	$this->load->model(array('Sms_setting_model')); 

        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }      
      } 
   
     public function index() { 
        $query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view('layout/header');
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view('layout/footer');
        //echo "hiii"; 
     }  
     /*public function add_view()
     {
	 	$this->load->helper('url'); 
        $this->load->view('layout/header');
        $this->load->view("$this->baseFolder/add");
        $this->load->view('layout/header');
	 }	
	 public function add()
	 {
	 	$type  		= NULL;
     	$sms    	= NULL;
     	$setDate= NULL;
     	$submit = NULL;
     	
     	extract($_POST);
     	$params['type']=$type;
     	$params['sms']=$sms;     	     	
     	$params['setDate']=date('Y-m-d', strtotime($setDate));
     	
     	if(isset($submit))
     	{			
			$res=$this->Sms_setting_model->insertData($params);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Added successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Failed to add');
			 }
		}		
        redirect('Sms_setting_controller/index');
	 }*/
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'type',
		'sms',
		'setDate'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Sms_setting_model->getUpdateData($data);
         $this->load->view('layout/header');
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view('layout/footer');
	 }
	  public function edit()
	 {	 
	 	$editId= NULL;
	 	$type   = NULL;
     	$sms    = NULL;
     	$setDate= NULL;
     	$submit = NULL;
     	
     	extract($_POST);
     	$editId=$editId;
     	$params['type']=$type;
     	$params['sms']=$sms;     	     	
     	$params['setDate']=date('Y-m-d', strtotime($setDate));
     	     	
     	if(isset($submit))
     	{			
			$res=$this->Sms_setting_model->updateAction($params,$editId);
			 if($res)
	         {
			 	$this->session->set_flashdata("flash",["type" => "success","message" => "Updated successfully"]);
			 }
			 else{
			 	$this->session->set_flashdata("flash",["type" => "danger","message" => "Updation failed"]);
			 }
		}		 
        redirect('Sms_setting_controller/index');
	 }
	 /*public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Sms_setting_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash",["type" => "success","message" => "Deleted successfully"]);
		 }
		 else{
		 	$this->session->set_flashdata("flash",["type" => "danger","message" => "Deletion failed"]);
		 }
        redirect('Sms_setting_controller/index');
      } */
   } 
