<?php 
defined('BASEPATH') OR exit('No direct script allowed');
class Card_controller extends CI_Controller{
	  
protected $baseFolder		=	'customer';
protected $table			=	'customer';
protected $tableCard		=	'card';
protected $tableInvoice		=	'point_details';
protected $header			=	'layout/header';
protected $footer			=	'layout/footer';
	  
public function __construct(){ 
	parent::__construct(); 
	$this->load->model(array('Card_model'));
	$this->load->model(array('Invoice_model'));
	$this->load->library('session');   
        
        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }     
} 
    
	public function add_card(){
		$cardId   		= NULL;
		$customerId		= NULL;
		$branchId		= NULL;
		$addedDate		= NULL;	 	
		$submit 		= NULL;
	     	
		extract($_POST);
		$params['cardId']		=	$cardId;
		$params['customerId']   =	$customerId; 
		$params['loginId']		=	$_SESSION['user_id'];      	
		$params['addedDate']	=	date('Y-m-d', strtotime($addedDate));
	     	
        // for sms sending like arabic and english 
     	//$smstype = $_POST['smstype']; 

	//validation for cardno already exist
	$result =    $this->Card_model->card_exists($cardId);//echo $result;die;
		 
	if($result>0){
		require('Customer_controller.php');
		$this->session->set_flashdata("flash",["type" => "danger","message" => "Card number already exist"]);
		redirect('Customer_controller/index');
		   	
	} 
		    	
	else if($result == 0){
		$lastInsertCardId=$mobile=$is_ok=0;
		if(isset($submit)){			
			$lastInsertCardId=$this->Card_model->insertData($params);
			if($lastInsertCardId){
				$sms['sendSms']=$this->Card_model->getSms(1);
				foreach($sms['sendSms'] as $q){
				   /*if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
				   if($smstype=="smsArabic") {
				   		$sendContent=$q->smsArabic;
				   }*/
				   $sendContent=$q->sms;
                   $smsType = $q->type; 				
				}
				$data['results']=$this->Card_model->getSmsSearchData($customerId,$lastInsertCardId);
				foreach($data['results'] as $r){
                    $cusId = $r->ID;
					$customerName=$r->customerName;
					$mobile=$r->mobile;
					$cardNo=$r->cardId;
					
				}
				
			function clean1($string,$cardNo) 
			{
	   			//echo $string; echo $cardNo;die;
			   $string = str_replace('$',$cardNo, $string); //Replaces all spaces with hyphens
			   return preg_replace('/[^A-Za-z0-9\-]/', '  ', $string);//Removes special chars.
			}				

			/*function clean2($string,$cardNo) {
			   			//echo $string; echo $cardNo;die;
			   $string = str_replace('B',$cardNo, $string); // Replaces all spaces with hyphens.
			    
			   return preg_replace('/B/', '  ', $string); // Removes special chars.
			}	*/				
				/*
				if($smstype=="smsEnglish") {
					$content = clean1($sendContent,$cardNo);//die;
				}
				if($smstype=="smsArabic") {
					$content = clean2($sendContent,$cardNo);//die;
				}*/
				$content = clean1($sendContent,$cardNo);//die;
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
            
					/* if($status === null) 
					{ 
					if($httpCode < 400) 
					{ 
					return TRUE; 
					} 
					else 
					{ 
					return FALSE; 
					} 
					} 
					elseif($status == $httpCode) 
					{ 
					return TRUE; 
					} 
            
					return FALSE; 
					pcntl_wait($status); */
				}
				
                    if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$content&priority=ndnd&stype=normal";
	
					$is_ok = http_response($url,$Message1);
				}


				//sms sending end
				if($is_ok && $lastInsertCardId)
				{
                    /*date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				$Count = $this->Card_model->saveData($values); //echo $Count;die;*/

					$this->session->set_flashdata("flash",["type" => "success","message" => "Card Added & SMS sent successfully"]);
					
				}
				else if($lastInsertCardId){
					$this->session->set_flashdata("flash",["type" => "success","message" => "Card added successfully"]);
				}
				else if($is_ok){
					$this->session->set_flashdata("flash",["type" => "success","message" => "SMS sent successfully"]);
				}
				else{
					$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to add"]);	
			}		
			redirect('Customer_controller/index');
			}       
          }       
	   }
	}	 
	public function delete(){ 
		$id = $this->uri->segment('3');
		//for getting details for log table 
		$details = $this->Card_model->getAllDetails($id); //print_r($details);die;
		//cusname if point is not added
		$cusName = $this->Card_model->getCusName($id);
		
			foreach($details as $d)
			{
				$cardNo = NULL;
				$tot_amount = NULL;
				$tot_point = NULL;
				$customerName = NULL;
				$addedDate = NULL;
				
				$data['cardNo'] = $d->cardNo;
				if($tot_amount || $tot_point)
				{
					$data['tot_amount'] = $d->totAmount;
					$data['tot_point'] = $d->totPoint;
					
				}
				else 
				{
					$data['tot_amount']= 0;
					$data['tot_point'] = 0;
					
				}
				$data['customerName'] = $cusName;
				$data['addedDate']	=	date('Y-m-d');
				//insert into log_Table
				$insetToLog=$this->Card_model->insertToLog($data);
			}
		
		
		$res=$this->Card_model->deleteData($id); 
		
		//delete invoice point details in card is delete
		//$res=$this->Invoice_model->deleteCardData($id); 
		if($res){
			$this->session->set_flashdata("flash",["type" => "success","message" => "Deleted successfully"]);
		}
		else{
			$this->session->set_flashdata("flash",["type" => "danger","message" => "Deletion failed"]);
		}        
		redirect('Customer_controller/index');
	} 

       public function edit_card(){
	$editId 		= NULL;	
	$cardId  		= NULL;
	$customerId		= NULL;
	$branchId		= NULL;
	$addedDate		= NULL;	 	
	$submit 		= NULL;
    
    $editId = $_POST['radio']; //echo $editId;die;
     	
	extract($_POST);
	
	$params['cardId']		=	$cardId; 
	$params['customerId']   =	$customerId;   
	$params['loginId']		=	$_SESSION['user_id'];      	
	$params['addedDate']	=	date('Y-m-d', strtotime($addedDate));
     	
        // for sms sending like arabic and english 
     	$smstype = $_POST['smstype'];

	//validation for cardno already exist
	$result =    $this->Card_model->card_exists($cardId);//echo $result;die;
		 
	if($result>0){
		require('Customer_controller.php');
		$this->session->set_flashdata('success_msg', 'card is already exists');
		redirect('Customer_controller/index');
		   	
	} 
		    	
	else if($result == 0){
		$lastInsertCardId=$mobile=$is_ok=0;
		if(isset($submit)){			
			$lastInsertCardId=$this->Card_model->updateAction($editId,$params);// var_dump($lastInsertCardId);die;
			if($lastInsertCardId){
				$sms['sendSms']=$this->Card_model->getSms(1);
				foreach($sms['sendSms'] as $q){
				   if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
				   if($smstype=="smsArabic") {
				   		$sendContent=$q->smsArabic;
				   }
                                  $smsType = $q->type; 				
				}
				$data['results']=$this->Card_model->getSmsSearchData($customerId,$lastInsertCardId);
				foreach($data['results'] as $r){
                                        $cusId = $r->ID;
					$customerName=$r->customerName;
					$mobile=$r->mobile1;
					$cardNo=$r->cardId;					
				}
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
            
					/* if($status === null) 
					{ 
					if($httpCode < 400) 
					{ 
					return TRUE; 
					} 
					else 
					{ 
					return FALSE; 
					} 
					} 
					elseif($status == $httpCode) 
					{ 
					return TRUE; 
					} 
            
					return FALSE; 
					pcntl_wait($status); */
				}
	
                            if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$sendContent&priority=ndnd&stype=normal";
	
					$is_ok = http_response($url,$Message1);
				}


				//sms sending end
				if($is_ok && $lastInsertCardId)
				{
                                        date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				$Count = $this->Card_model->saveData($values); //echo $Count;die;

					$this->session->set_flashdata('success_msg', 'Card Added & SMS sent successfully');
				}
				else if($lastInsertCardId){
					$this->session->set_flashdata('success_msg', 'Card added successfully');
				}
				else if($is_ok){
					$this->session->set_flashdata('success_msg', 'SMS sent successfully');
				}
				else{
					$this->session->set_flashdata('success_msg', 'Failed to add');
				}
			}		
			redirect('Customer_controller/index');
		}       
       }       
	}
 
}