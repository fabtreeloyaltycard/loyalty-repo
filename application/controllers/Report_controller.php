<?php 
defined('BASEPATH') OR exit('No direct script allowed');
class Report_controller extends CI_Controller{
	protected $baseFolder		=	'report';
	protected $table4			=	'point_setting';
	protected $header			=	'layout/header';
	protected $footer			=	'layout/footer';
	  
	public function __construct(){ 
		parent::__construct(); 
		$this->load->model(array('Report_model')); 
                $this->load->model(array('Redeem_model')); 
		$this->load->library('session'); 

                if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }              
	} 
	
      public function redeem_report(){	
		$data['results']=$this->Report_model->getredeemData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/redeem_report",$data);
        $this->load->view("$this->footer");
	}
	
	
	//showing index page
	
	public function index(){	
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/new_branch_report_index");
        $this->load->view("$this->footer");
	}
	
	
	
	public function search(){
		$cardNo   		= NULL;
		$customerName   = NULL;
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName; 
		$data['phone'] 		  = $phone;	  	        
		if($data){
			$datas['results1']=$this->Report_model->getBranchSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/new_branch_report_getData",$datas);		
	}
      
        //for purchase return
	
	public function purchase_return_report(){	
		$data['results']=$this->Report_model->getAllPurReData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/purchase_return_report",$data);
        $this->load->view("$this->footer");
	}

} 
