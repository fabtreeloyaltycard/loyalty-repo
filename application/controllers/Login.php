<?php
class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }
    public function index () {
        if (@$_SESSION['logged_in'] && @$_SESSION['user_type'] == "admin"  || @$_SESSION['user_type'] == "branch") {
            $this->load->view('layout/header');
            $this->load->view('welcome_message');
            $this->load->view('layout/footer');
        } else {
            $this->load->helper('form');
            $this->load->view('index');
        }
    }
    public function login() {
        // create the data object
        $data = new stdClass();
        if (@$_SESSION['logged_in'] && @$_SESSION['user_type'] == "admin") {
            $this->load->view('layout/header');
            $this->load->view('welcome_message', $data);
            $this->load->view('layout/footer');
        } else {

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->library('form_validation');

            // set validation rules
            $this->form_validation->set_rules('userName', 'Username', 'required|alpha_numeric');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == false) {

                // validation not ok, send validation errors to the view
                $this->load->view('index');

            } else {

                // set variables from the form
                $userName = $this->input->post('userName');
                $password = $this->input->post('password');
                if ($this->login_model->resolve_user_login($userName, $password)) {

                    $user_id = $this->login_model->get_user_id_from_username($userName);
                    $user    = $this->login_model->get_user($user_id);

                    // set session user datas
                    $_SESSION['user_id']      = (int)$user->id;
                    $_SESSION['username']     = (string)$user->userName;
                    $_SESSION['logged_in']    = (bool)true;
                    $_SESSION['is_confirmed'] = (bool)true;
                    $_SESSION['user_type']     = (string)$user->userType;

                    // user login ok
                    //redirect(base_url().'index.php/welcome');
                    $this->load->view('layout/header');
                    $this->load->view('welcome_message', $data);
                    $this->load->view('layout/footer');

                } else {

                    // login failed
                    $data->error = 'Wrong User Name or password.';

                    // send error to the view
                    $this->load->view('index', $data);

                }

            }
        }

    }
    
    
    public function adminDashboard()
	{
		$this->load->view('layout/header');
		$this->load->view('welcome_message');
		$this->load->view('layout/footer');
	}
    
    public function logout() {

        // create the data object
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            $this->load->view('index', $data);

        } else {

            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            //$this->load->view('index', $data);
            redirect('/');
        }

    }
    public function changePasswordForm () {
        if (@$_SESSION['logged_in']) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('layout/header');
            $this->load->view('change-password');
            $this->load->view('layout/footer');
        } else {
            redirect('/');
        }
    }
    public function changePassword () {
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nPassword', 'New Password', 'required');
        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required');
        if ($this->form_validation->run() == false) {
            // validation not ok, send validation errors to the view
            $this->load->view('layout/header');
            $this->load->view('change-password');
            $this->load->view('layout/footer');
        } else {
            $currentPassword = $this->input->post('password');
            $newPassword = $this->input->post('nPassword');
            $confirmPassword = $this->input->post('cPassword');
            if ($newPassword != $confirmPassword) {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Password mismatch!"]);
                $this->load->view('layout/header');
                $this->load->view('change-password');
                $this->load->view('layout/footer');
            } else {
                $user = $this->login_model->get_user($_SESSION['user_id']);
                if ($this->login_model->checkPasswordMatch($currentPassword, $user->password)) {
                    if ($this->login_model->updatePassword($_SESSION['user_id'], $newPassword)) {
                        redirect(base_url().'index.php/login/logout');
                    } else {
                        $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Something went wrong! Please try again."]);
                        $this->load->view('layout/header');
                        $this->load->view('change-password');
                        $this->load->view('layout/footer');
                    }
                } else {
                    $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Current password is incorrect!"]);
                    $this->load->view('layout/header');
                    $this->load->view('change-password');
                    $this->load->view('layout/footer');
                }
            }
        }
    } //end of function
    
    //reset password for admin
    public function reset_password() 
    {
		if (@$_SESSION['logged_in']) {
			
			$res['records'] = $this->login_model->getAllData();
			
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('layout/header');
            $this->load->view('reset_password/index',$res);
            $this->load->view('layout/footer');
        } else {
            redirect('/');
        }
	}
	//for getting username when selecting the branchName		
	 public function getAjax() {
		  	$this->load->view('reset_password/getBranchAjax');
	  }
	  
	  public function changeBranchPassword()
	  {
	  		$this->load->helper('form');
	        $this->load->library('form_validation');

	        // set validation rules
	        $this->form_validation->set_rules('nPassword', 'New Password', 'required');
	        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required');
	        if ($this->form_validation->run() == false) 
	        {
            // validation not ok, send validation errors to the view
            $this->load->view('layout/header');
            $this->load->view('reset_password/index');
            $this->load->view('layout/footer');
        	}
        	else
        	{
				$newPassword = $this->input->post('nPassword');
            	$confirmPassword = $this->input->post('cPassword');
            	if ($newPassword != $confirmPassword) 
            	{
	                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Password mismatch!"]);
	                $this->load->view('layout/header');
	                $this->load->view('reset_password/index');
	                $this->load->view('layout/footer');
            	} // end of if
            	else
            	{
					$branchId = $this->input->post('branchList');
					
					$res = $this->login_model->updateBranchPass($branchId,$newPassword);
					if(isset($res))
					{
						redirect(base_url().'index.php/login/logout');
					}
					else
					{
						$this->session->set_flashdata("flash", ["type" => "danger", "message" => "Something went wrong! Please try again."]);
                        $this->load->view('layout/header');
                        $this->load->view('reset_password/index');
                        $this->load->view('layout/footer');
					}
				}
			} //end of else
	        
	  } //end of function
	
}