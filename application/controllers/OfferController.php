<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class OfferController extends CI_Controller {
	  protected $baseFolder		=	'Offers';
	  protected $subFolder		=	'report';
	  protected $table			=	'offer_details';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
      protected $condition = 1;
	  
      public function __construct() { 
         parent::__construct(); 
         $this->load->model(array('OfferModel'));
         $this->load->helper('form');
    	 $this->load->library('form_validation');
         $this->load->library('session'); 
         if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }       
      } 
   
     public function index() { //$offset is for pagination
     
     	$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/OfferController/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 10;
		
	
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     
        $query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        $data['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer"); 
     }  
     
      public function add_view()
     {
	 	$this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 
	 
	 public function add()
	 {
	 	$offer  				= NULL;
     	$offerDescription  		= NULL;
     	$offerSetDate			= NULL;
     	$offerToDate			= NULL;
     	
     	extract($_POST);
     	$params['offer']			=	$offer;
     	$params['offerDescription'] =	$offerDescription; 
     	$params['offerSetDate']		=	date('Y-m-d', strtotime($offerSetDate));
     	$params['offerToDate']		=	date('Y-m-d', strtotime($offerToDate));
     	
     	$this->form_validation->set_rules('offer', 'Offer', 'required');
     	$this->form_validation->set_rules('offerSetDate', 'offer Set Date', 'required');
     	
        //offer name alredy exists validation
     	$offerCount = $this->OfferModel->offer_exists($offer);//echo $offerCount;die;
     	
     	if($offerCount>0){
			$this->session->set_flashdata("flash",["type" => "danger","message" => "Offer Name is already exists"]);
			redirect('OfferController/add_view');
		}
     	
     	else {     	
           

     	if(isset($submit))
     	{			
			$res=$this->OfferModel->insertData($params);
			 if($res)
	         {
			 	$this->session->set_flashdata("flash",["type" => "success","message" => "Added successfully"]);
			 }
			 else{
			 	$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to add"]);
			 }
		}
	}	
		if ($this->form_validation->run() == false) {			
         redirect('OfferController/add_view');
         }
     	
		redirect('OfferController/index'); 
	 }
	 
	 
	
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'offer',
		'offerDescription',
		'offerSetDate',
		'offerToDate'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->OfferModel->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	   public function edit()
	 {	 
	 	$editId= NULL;
	 	$offer   = NULL;
     	$offerDescription    = NULL;
     	$offerSetDate= NULL;
     	$offerToDate= NULL;
     	$submit = NULL;
     	
     	extract($_POST);
     	$editId=$editId;
     	$params['offer']=$offer;
     	$params['offerDescription']=$offerDescription;     	
     	$params['offerSetDate']=date('Y-m-d', strtotime($offerSetDate));
     	$params['offerToDate']=date('Y-m-d', strtotime($offerToDate));
     	     	
     	if(isset($submit))
     	{			
			$res=$this->OfferModel->updateAction($params,$editId);
			 if($res)
	         {
			 	$this->session->set_flashdata("flash",["type" => "success","message" => "Updated successfully"]);
			 }
			 else{
			 	$this->session->set_flashdata("flash",["type" => "danger","message" => "Updation failed"]);
			 }
		}		 
        redirect('OfferController/index');
	  }
	 
	 public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->OfferModel->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash",["type" => "success","message" => "Deleted successfully"]);
		 }
		 else{
		 	$this->session->set_flashdata("flash",["type" => "danger","message" => "Deletion failed"]);
		 }
         redirect('OfferController/index');
      } 
      
      //for report of offer details
      public function report_index() { 
        $offerName = $this->input->post('offerName');
        $offerDate = $this->input->post('offerDate');
        $offerToDate = $this->input->post('offerToDate');
        if($offerName)
        {
          // $this->condition = $this->condition.' AND offer like"%'.$offerName.'%"' ;
           $this->db->like($this->table.'.offer', $offerName);
        }
         if($offerDate)
        {
           //$this->condition = $this->condition.' AND offerSetDate like"%'.$offerDate.'%"' ;
            $offerDate = date('Y-m-d', strtotime($offerDate));
            $this->db->where($this->table.'.offerSetDate', $offerDate);
        }
		 if($offerToDate)
        {
           //$this->condition = $this->condition.' AND offerSetDate like"%'.$offerDate.'%"' ;
            $offerToDate = date('Y-m-d', strtotime($offerToDate));
            $this->db->where($this->table.'.offerToDate', $offerToDate);
        }
		
		
        $query = $this->db->get("$this->table"); 
        //echo $this->db->last_query();
        $data2['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->subFolder/offer_report",$data2);
        $this->load->view("$this->footer"); 
     }  
      
      
   } 
