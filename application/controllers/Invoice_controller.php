<?php 
defined('BASEPATH') OR exit('No direct script allowed');
class Invoice_controller extends CI_Controller{
	protected $baseFolder		=	'invoice';
	/*protected $table1			=	'point_details';
	protected $table2			=	'customer';
	protected $table3			=	'card';*/
	//protected $table4			=	'point_setting';
	protected $header			=	'layout/header';
	protected $footer			=	'layout/footer';
	  
	public function __construct(){ 
		parent::__construct(); 
		$this->load->model(array('Invoice_model')); 
		$this->load->library('session');  
           
                if(empty($this->session->userdata("user_id")))
                {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
                }             
	} 
	public function index(){	
		/*$this->load->view("$this->header");
		$this->load->view("$this->baseFolder/index");
		$this->load->view("$this->footer");*/
		//$data['results']=$this->Invoice_model->getAllData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index");
        $this->load->view("$this->footer");
	}
	
	public function search(){
		$cardNo   		= NULL;
		$customerName   = NULL;
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName;
		$data['phone'] 		  = $phone;
	        
		if($data){
			$datas['results1']=$this->Invoice_model->getSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/getData",$datas);		
	}
  
	public function add()
	 {
	 	$customerId   = NULL;
     	$cardId    	  = NULL;
     	$amount		  = NULL;
     	$submit       = NULL;
     	$point		  = NULL;
     	
     	extract($_POST);
		$params['customerId']	=	$customerId;
     	$params['cardId']		=	$cardId; 
     	$params['amount']		=	$amount;
     	$params['point']		=	$point;
     	$params['loginId']		=	$_SESSION['user_id'];     	
     	$params['addedDate']	=	date('Y-m-d');

        //for getting the smstype (english or arabic)
     	//$smstype = $_POST['smstype'];

     	$lastInsertCardId=$customerName=$mobile=$cardNumber=$point=$sendContent=$is_ok=$Message1 =0;
     	if(isset($submit))
     	{			
			 $lastInsertCardId=$this->Invoice_model->insertData($params);
			if($lastInsertCardId){
				$sms['sendSms']=$this->Invoice_model->getSms(2);
				foreach($sms['sendSms'] as $q){
					
					/*if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
					if($smstype=="smsArabic") {
						$sendContent=$q->smsArabic;
					}*/
					$sendContent=$q->sms;	
                  	$smsType = $q->type;  				
				} 
				$data['results']=$this->Invoice_model->getSmsSearchData($lastInsertCardId);
				foreach($data['results'] as $r){
                    $cusId = $r->ID;
                    /*if($smstype=="smsEnglish") {
						$customerName=$r->customerName;	
					}
					if($smstype=="smsArabic") {
						$customerName=$r->customerNameArabic;	
					}*/
					$customerName=$r->customerName;	
					$mobile=$r->mobile;
					$cardNumber=$r->cardNumber;
					$point=$r->point;	
				}
				
				//function for getting points in english content
				function clean1($string,$point,$customerName,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('@',$point, $string);
   					$string2 = str_replace('&',$customerName, $string1);
   					$string3 = str_replace('$',$cardNumber, $string2);
   					return $string3;
				}				
				
				/*//function for getting points in arabic content
				function clean2($string,$point,$customerName,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('A',$point, $string);
   					$string2 = str_replace('C',$customerName, $string1);
   					$string3 = str_replace('B',$cardNumber, $string2);
   					return $string3;
				}*/
				
				
				$content = clean1($sendContent,$point,$customerName,$cardNumber);//die;		
				
				/*if($smstype=="smsArabic") {
					//$sendContent=$sendContent." اسم الزبون:".$customerName.", لا بطاقة:".$cardNumber;	
//echo $sendContent;die;
					//calling function clean2()
					$content = clean2($sendContent,$point,$customerName,$cardNumber);//die;		
				}	*/
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}

                     if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$content&priority=ndnd&stype=normal";
	
					$is_ok = http_response($url,$Message1);
				}

				//sms sending end
				if($is_ok && $lastInsertCardId)
				{
                    /*date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				$Count = $this->Invoice_model->saveData($values); //echo $Count;die;*/

					$this->session->set_flashdata("flash",["type" => "success","message" => "Point Added & SMS sent successfully"]);
					
				}
				else if($lastInsertCardId){
					$this->session->set_flashdata("flash",["type" => "success","message" => "Point added successfully"]);
				}
				else if($is_ok){
					$this->session->set_flashdata("flash",["type" => "success","message" => "SMS sent successfully"]);
				}
				else{
					$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to add"]);
				}
			}
		}		
        redirect('Invoice_controller/index');
	 }
	public function calculatePoint()
	 {
	 	$point=$j=0;
	 	$amount   = NULL;
	 	extract($_POST);
	 	//$query = $this->db->get("$this->table4"); 
        //$data4['records'] = $query->result();
        
        if($amount!='')
        { 
        	$point = ($amount*1)/100;
		}
		echo $point;
	 }
	 
	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Invoice_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
        redirect('Invoice_controller/report');
      } 
      
      public function report(){	
		$data['results']=$this->Invoice_model->getAllData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/report",$data);
        $this->load->view("$this->footer");
	}
} 
