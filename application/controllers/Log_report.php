<?php 
   class Log_report extends CI_Controller {
	  
      public function __construct() { 
         parent::__construct(); 
         
         $this->load->library('session');
         $this->load->model('Card_model'); 
         if(empty($this->session->userdata("user_id")))
         {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
         }       
       } 
   
     public function index() { //$offset is for pagination
     
     	$customerName = $this->input->post('customerName');
        $addedDate = $this->input->post('addedDate');
        $cardNo = $this->input->post('cardNo');
        if($customerName)
        {
           // $this->condition = $this->condition.' AND offer like"%'.$offerName.'%"' ;
           $this->db->like('log_table.customerName', $customerName);
        }
         if($addedDate)
        {
           //$this->condition = $this->condition.' AND offerSetDate like"%'.$offerDate.'%"' ;
            $addedDate = date('Y-m-d', strtotime($addedDate));
            $this->db->where('log_table.addedDate', $addedDate);
        }
		 if($cardNo)
        {
           //$this->condition = $this->condition.' AND offerSetDate like"%'.$offerDate.'%"' ;
            $this->db->like('log_table.cardNo', $cardNo);
        }
		
		
        $query = $this->db->get('log_table'); 
        //echo $this->db->last_query();
        $data2['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view('layout/header');
        $this->load->view('log/log_report',$data2);
        $this->load->view('layout/footer');  
     }
     
     public function delete($id)
     {
	 	$res=$this->Card_model->deleteLogData($id); 
	 	if($res){
			$this->session->set_flashdata("flash",["type" => "success","message" => "Deleted successfully"]);
		}
		else{
			$this->session->set_flashdata("flash",["type" => "danger","message" => "Deletion failed"]);
		}        
		redirect('Log_report/index');
	 }
      
   } 
