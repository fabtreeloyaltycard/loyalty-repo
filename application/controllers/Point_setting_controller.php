<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Point_setting_controller extends CI_Controller {
	  protected $baseFolder		=	'point_setting';
	  protected $table			=	'point_setting';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
	  
      public function __construct() { 
         parent::__construct(); 
         $this->load->model(array('Point_setting_model'));
         $this->load->library('session');  

         if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }      
      } 
   
     public function index() { 
        $query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
        //echo "hiii"; 
     }  
     
	
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'amountFrom',
		'amountTo',
		'percentage',
		'amount'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Point_setting_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId = NULL;
	 	$amountFrom = NULL;
	 	$amountTo = NULL;
     	$percentage    = NULL;     	
     	$amount    = NULL;     	
     	$submit = NULL;
     	
     	extract($_POST);
     	$editId=$editId;
     	$params['amountFrom']=$amountFrom;
     	$params['amountTo']=$amountTo;
     	$params['percentage']=$percentage;
     	$params['amount']=$amount;     	
     	
     	if(isset($submit))
     	{			
			$res=$this->Point_setting_model->updateAction($params,$editId);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Updated successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Updation failed');
			 }
		}
		/*$query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
		$this->load->view("$this->header");		
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");*/
        redirect('Point_setting_controller/index');
	 }
	 public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Point_setting_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
        
   		/*$query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
		$this->load->view("$this->header");		
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");*/
         redirect('Point_setting_controller/index');
      } 
   } 
 