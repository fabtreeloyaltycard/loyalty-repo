<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Sms_controller extends CI_Controller {
	  protected $baseFolder		=	'sms';
	  protected $table			=	'sms_setting';
	  protected $table2			=	'customer';
	  protected $table3			=	'card';
      protected $table4			=	'sms_send_details';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
	  
      public function __construct() { 
         parent::__construct(); 
         $this->load->model(array('Sms_model'));
         $this->load->library('session');  

         if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }      
      } 
   
     public function index() {        
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index");
        $this->load->view("$this->footer");
        //echo "hiii"; 
     } 
      public function broadcast() {
      	$loginId = $_SESSION['user_id']; //print_r($loginId);die;
	   	$loginType = $_SESSION['user_type'];
	   	
	   	$num_rows=$this->db->count_all("$this->table2");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Sms_controller/broadcast';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 10;
		
		//$config['num_links'] = 2;
    
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	   	
      	$data['sendSms']=$this->Sms_model->getSms(5);
      	if($loginType!="admin") {
      		$where = "$this->table2.loginId='$loginId'";
      		$this->db->where($where);
      		
      		$query = $this->db->get("$this->table2",$config['per_page'],$this->uri->segment(3)); 
      	}        
        else {
        	$data['sendSms']=$this->Sms_model->getSms(5);
			$query = $this->db->get("$this->table2",$config['per_page'],$this->uri->segment(3)); 
		}
        $data['records'] = $query->result(); 
        $query = $this->db->get("$this->table3"); 
        $data['cardRecords'] = $query->result(); //print_r($data);
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/broadcast",$data);
        $this->load->view("$this->footer");
     }
     
     //for individual index page searching
     public function search(){
		$cardNo   		= NULL;
		$customerName   = NULL;
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName;
		$data['phone'] 		  = $phone;	        
		if($data){
			$datas['results1']=$this->Sms_model->getSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/getData",$datas);		
	} 
    
    //for inddividual sms sending
	public function send(){
	$customerName	= NULL;
	$customerId		= NULL;
	$mobile 		= NULL;
	$submit 		= NULL;
     	
	extract($_POST);
	
	$mobile		=	$mobile;   
	
	$cusId = 0;
	//$smstype = $_POST['smstype'];  	
    
		if(isset($submit)){		
				$sms['sendSms']=$this->Sms_model->getSms(4);
				foreach($sms['sendSms'] as $q){
					/*if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
					//echo $sendContent;die;
					if($smstype=="smsArabic") {
						$sendContent=$q->smsArabic;
					}*/
					$sendContent=$q->sms;	
                  	$smsType = $q->type;				
				} 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}
				
	
				if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$sendContent&priority=ndnd&stype=normal";
					//echo $Message1;die;
	
					$is_ok = http_response($url,$Message1);
				}
				
				if($is_ok){
					
					/*date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;
					
					//offer name alredy exists validation
     				$Count = $this->Sms_model->saveSmsData($values); //echo $Count;die;*/
					$this->session->set_flashdata("flash",["type" => "success","message" => "SMS sent successfully"]);
				}
				else{
					$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to send"]);
				}
			}		
			redirect('Sms_controller/index');
		} 
	
	  //for the action of broadcastSms sending.	      
      public function broadcastSms(){	
				$cusId = 0;
				$sms['sendSms']=$this->Sms_model->getSmsBroadcast(5);
				foreach($sms['sendSms'] as $q){
					/*if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
					//echo $sendContent;die;
					if($smstype=="smsArabic") {
						$sendContent=$q->smsArabic;
					}*/
					$sendContent=$q->sms;	
                  	$smsType = $q->type; 				
				}
				
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}
				

				$userType = $_SESSION['user_type'];
			    $userId = $_SESSION['user_id'];
				
				if($userType!="admin") {
					$where = "customer.loginId='$userId'";
					$this->db->where($where);
					$query = $this->db->get("$this->table2");
					//echo $this->db->last_query();die;
				}
				else {
					$query = $this->db->get("$this->table2");
				}
 
        		$records = $query->result();
        		foreach($records as $q2){
					$mobile=$q2->mobile;				
				
					if($mobile and $sendContent){

						$url ="http://sms.git.ind.in/api/sendmsg.php";

						$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$sendContent&priority=ndnd&stype=normal";
						//echo $Message1;
		
						$is_ok = http_response($url,$Message1);
					}
				}
			
			
				//sms sending end
				
				if($is_ok){
					
					/*date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				$Count = $this->Sms_model->saveBroadData($values); //echo $Count;die;*/
					
					$this->session->set_flashdata("flash",["type" => "success","message" => "SMS sent successfully"]);
				}
				else{
					$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to send"]);
				}		
			redirect('Sms_controller/broadcast');
		} 

               // for sms set details table index
		public function sentDetails() {
		
		$num_rows=$this->db->count_all("$this->table4");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Sms_controller/sentDetails';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['num_links'] = 2;
    
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
		
		
			
		$loginId = $_SESSION['user_id'];
		$loginType = $_SESSION['user_type'];
		
		$this->db->select($this->table4.'.*');
		//$this->db->from($this->table4);
		
		if($loginType!="admin") {
			$where = $this->table4.'.loginId='.$loginId;
			$this->db->where($where);
			$this->db->order_by($this->table4.'.ID');
			
		}
		else {
			$this->db->order_by($this->table4.'.ID');
		}
		$query = $this->db->get("$this->table4",$config['per_page'],$this->uri->segment(3));
		
		$data['records'] = $query->result(); 
			
			$this->load->helper('url'); 
       		$this->load->view("$this->header");
        	$this->load->view("$this->baseFolder/sms_sent_details",$data);
        	$this->load->view("$this->footer"); 
		}
		
		public function deleteSmsSent(){
			
			$id = $this->uri->segment('3'); //echo $id;die;
			$res = $this->Sms_model->delete_sms_sent_details($id);
			if($res)
         	{
		 		$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 	}
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
		 redirect('Sms_controller/sentDetails');
			 
		} 

              
                // * *************for special Sms
		
	public function special_index() {        
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/special_index");
        $this->load->view("$this->footer");
        //echo "hiii"; 
     } 
     
     
     public function special_search(){
		$cardNo   		= NULL;
		$customerName   = NULL; 
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName;
		$data['phone'] 		  = $phone;	        
		if($data){
			$datas['results1']=$this->Sms_model->getSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/getSpecialData",$datas);		
	} 
	
	public function special_send(){
	$customerName	= NULL;
	$customerId		= NULL;
	$mobile			= NULL;
	$specialSms		= NULL;
	$submit 		= NULL;
     	
	extract($_POST);
	
	$mobile		=	$mobile;   
	
	$cusId = 0;
	$sendContent = '';
		if(isset($submit)){		
				$sms['sendSms']=$this->Sms_model->getSms(4);
				foreach($sms['sendSms'] as $q){
					$sendContent=$specialSms;				
				}
				$sendContent.=',Customer:'.$customerName;
				
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}
				
	
				if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$sendContent&priority=ndnd&stype=normal";
					//echo $Message1;die;
	
					$is_ok = http_response($url,$Message1);
				}
				
				if($is_ok){
					
					/*date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;
					
					//offer name alredy exists validation
     				$Count = $this->Sms_model->saveSmsData($values); //echo $Count;die;*/
					$this->session->set_flashdata("flash",["type" => "success","message" => "SMS sent successfully"]);
				}
				else{
					$this->session->set_flashdata("flash",["type" => "danger","message" => "Failed to Sent"]);
				}
			}		
			redirect('Sms_controller/special_index');
		}
	


      
   } 
