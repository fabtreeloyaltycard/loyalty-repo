$(function () {
    // Show file upload thumbnail before submitting the form
    function showThumbnail(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $(input).nextAll('.image_upload_preview').remove();
            reader.onload = function (e) {
                $(input).after('<img class="image_upload_preview" src="' + e.target.result + '" alt=""/>');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', 'input[type="file"][data-action="show_thumbnail"]', function () {
        showThumbnail($(this).get(0));
    });

    // Hiding session message after a few seconds
    setTimeout(function () {
        $('div[data-role="auto-hide"]').fadeOut();
    }, 5000);
    $(document).on('click', 'div[data-role="auto-hide"]', function () {
        $(this).fadeOut();
    });

    // Triggering submit the hidden forms
    $(document).on('click', 'a[data-action="trigger_form"]', function(e) {
        e.preventDefault();
        $(this).find('form.hidden_form').submit();
    });
    //date picker 
    $('.datepicker').datepicker({
   	format: 'dd-mm-yyyy'
   });

    // Redeem point validation
    $(document).on('keyup input', '[data-role="redeem_input"]', function () {
        var redeemInputField = $(this),
            redeemInput = redeemInputField.val(),
            redeemMax = parseFloat(redeemInputField.parent().prev().text());
        if (!isNaN(redeemInput) && redeemInput) {
            if (redeemInput > redeemMax && redeemInput > 0) {
                redeemInputField.val(redeemMax);
            } else if (redeemInput < 0) {
                alert('Please enter a valid number!');
                redeemInputField.val('');
            }
        } else if (isNaN(redeemInput)) {
            alert('Please enter a valid number!');
            redeemInputField.val('');
        }
    });
    // Invoice amount validation
    $(document).on('keyup input', '[data-role="restrict_neg"]', function () {
        var invInputField = $(this),
            invInput = invInputField.val();
        if (!isNaN(invInput) && invInput) {
            if (invInput < 0) {
                console.log(1);
                alert('Please enter a valid number!');
                invInputField.val('');
            }
        } else if (isNaN(invInput)) {
            console.log(1);
            alert('Please enter a valid number!');
            invInputField.val('');
        }
    });
    
});